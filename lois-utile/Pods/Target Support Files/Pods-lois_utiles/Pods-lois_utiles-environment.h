
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// HanekeSwift
#define COCOAPODS_POD_AVAILABLE_HanekeSwift
#define COCOAPODS_VERSION_MAJOR_HanekeSwift 0
#define COCOAPODS_VERSION_MINOR_HanekeSwift 9
#define COCOAPODS_VERSION_PATCH_HanekeSwift 1

// Spring
#define COCOAPODS_POD_AVAILABLE_Spring
#define COCOAPODS_VERSION_MAJOR_Spring 1
#define COCOAPODS_VERSION_MINOR_Spring 0
#define COCOAPODS_VERSION_PATCH_Spring 3

// SwiftyJSON
#define COCOAPODS_POD_AVAILABLE_SwiftyJSON
#define COCOAPODS_VERSION_MAJOR_SwiftyJSON 2
#define COCOAPODS_VERSION_MINOR_SwiftyJSON 2
#define COCOAPODS_VERSION_PATCH_SwiftyJSON 0

// TTTAttributedLabel
#define COCOAPODS_POD_AVAILABLE_TTTAttributedLabel
#define COCOAPODS_VERSION_MAJOR_TTTAttributedLabel 1
#define COCOAPODS_VERSION_MINOR_TTTAttributedLabel 13
#define COCOAPODS_VERSION_PATCH_TTTAttributedLabel 3


//
//  protocol.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 02/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import Foundation

protocol Serializable {
    init(dictionary: [NSObject: AnyObject])
}

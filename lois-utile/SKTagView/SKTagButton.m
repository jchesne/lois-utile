//
// Created by Shaokang Zhao on 15/1/12.
// Copyright (c) 2015 Shaokang Zhao. All rights reserved.
//

#import "SKTagButton.h"
#import "SKTag.h"

@implementation SKTagButton

+ (instancetype)buttonWithTag:(SKTag *)tag
{
    SKTagButton *btn = [super buttonWithType:UIButtonTypeSystem];
    [btn setTitle:tag.text forState:UIControlStateNormal];
    [btn setTitleColor:tag.textColor forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:tag.fontSize];


    btn.contentEdgeInsets = tag.padding;
    
    btn.isActive = tag.isActive;
    
    if(tag.isActive){
        [btn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
        btn.backgroundColor = [UIColor colorWithRed:59.0f/255.0f
                                              green:77.0f/255.0f
                                               blue:79.0f/255.0f
                                              alpha:1.0f];
        btn.layer.borderColor = tag.borderColor.CGColor;
    }
    else{
        [btn setTitleColor:[UIColor colorWithRed:59.0f/255.0f
                                           green:77.0f/255.0f
                                            blue:79.0f/255.0f
                                           alpha:1.0f] forState:UIControlStateNormal];
        btn.backgroundColor = [UIColor whiteColor];
    }
    
    if (tag.bgImg)
    {
        [btn setBackgroundImage:tag.bgImg forState:UIControlStateNormal];
    }
    
    if (tag.borderColor)
    {
        btn.layer.borderColor = tag.borderColor.CGColor;
    }
    
    if (tag.borderWidth)
    {
        btn.layer.borderWidth = tag.borderWidth;
    }
    

    
    btn.layer.cornerRadius = tag.cornerRadius;
    btn.layer.masksToBounds = YES;
    
    return btn;
}

- (void)switchState
{
    self.isActive = !self.isActive;
    
    if(self.isActive){
        [self setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor colorWithRed:59.0f/255.0f
                                               green:77.0f/255.0f
                                                blue:79.0f/255.0f
                                               alpha:1.0f];
    }
    else{
        [self setTitleColor:[UIColor colorWithRed:59.0f/255.0f
                                            green:77.0f/255.0f
                                             blue:79.0f/255.0f
                                            alpha:1.0f] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor whiteColor];
    }
}

- (void)setDisplay {
    // Implementation goes here
}

@end

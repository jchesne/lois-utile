//
//  Twitter.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 12/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//


import Foundation
import UIKit
import Realm
import SwifteriOS

let TwitterClass: Twitter = Twitter()

class Twitter{
    
    var swifter: Swifter
    let useACAccount = false
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    init(){
        self.swifter = Swifter(consumerKey: "JoM4bx9uBGMoRpLIc5owK84ng", consumerSecret: "5F1c2U9o3iuNZ4RLLgilvjHWkHLwRbZwzbvlHEWsWRX1qQd8dp")
    }
    
    func initSession(userInit:Bool = true,callback: () -> Void){
        // Je connecte l'utilisateur en utilisant le token twitter.
        

        var key = ""
        var secret = ""
        
        // J'initialise la session twitter via le user connecté avec Twitter.
        if(userInit){
            var connectedUser = UserModel.getUser()!
            key = connectedUser.twitterkey
            secret = connectedUser.twittersecret
        }
        // J'initialise la session twitter via le temp_share déjà acquis.
        else{
            var twitter_share_temp = tempShare.getTempShare()!
            key = twitter_share_temp.share_temp_twitterkey
            secret = twitter_share_temp.share_temp_twittersecret
        }
        
        let accessToken = SwifterCredential.OAuthAccessToken(key: key, secret: secret)
        let credential = SwifterCredential(accessToken: accessToken)

        swifter.client.credential = credential
        
        
        callback()
    }
    
    func login(useToken:Bool = false, displayLoader:Bool = false){
        var accessToken = swifter.client.credential?.accessToken!
        var connectedUser = UserModel.getUser()
        var screenName = ""
        var userID = ""
        //@TODO: Ici vérifier si tout est bon, j'ai déjà eu une erreur suite à un login Twitter.
        if(connectedUser != nil){
            if(connectedUser?.twitterUID != ""){
                screenName = connectedUser!.twitterScreenName
                userID = connectedUser!.twitterUID
            }
            else{
                screenName = accessToken!.screenName!
                userID = accessToken!.userID!
            }
        }
        else{
            screenName = accessToken!.screenName!
            userID = accessToken!.userID!
        }
        
        var params = [
            "type":"twitter",
            "account":[[
                "twitter_id":userID,
                "key":accessToken!.key,
                "secret":accessToken!.secret,
                "twitter_username":screenName,
                "twitter_email":"customemail@customemail.com",
                ]]
        ]
        
        var err: NSError?
        var data = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        
        if(displayLoader){
            var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel("Connexion en cours...",options: options)
        }
        
        self.post(useToken: useToken,type:"registerTwitterUser",params: data!, url: "\(self.appDelegate.url)/api/v1/fr/user/createlogin") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {

                datasSingleton.sharedInstance.userFavorites()
               // datasSingleton.sharedInstance.downloadAvatar()
                
                //Je met à jour le user avec le token twitter uniquement si le user ne l'a pas déjà.
                var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                if(connectedUser.twitterUID == ""){
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    connectedUser.twittertoken = params["account"]![0]["key"] as! String
                    connectedUser.twitterkey = params["account"]![0]["key"] as! String
                    connectedUser.twittersecret = params["account"]![0]["secret"] as! String
                    connectedUser.twitterUID = params["account"]![0]["twitter_id"] as! String
                    realm.commitWriteTransaction()
                }
                
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    func postToTwitter(lois:Lois!){
        //Je regarde si user connecté
        //Si connecté via Twitter et si credential ok (à priori oui)
        
        //Si non connecté, regarde si temps share existe
            //Si il existe alors je fais une mise à jour du credential et je share
            //Sinon je fais la demande d'authorisation
            //À la réponse, je met à jour le temps
        
        //EN fait je fais la demande que si je ne suis pas connecté via twitter et que temp share est vide.
        
        if(UserModel.getUser() != nil){
            if(UserModel.getUser()?.twittertoken != ""){
                
                println("Je partage avec la session twitter de la connection Twitter")
                
                TwitterClass.initSession(userInit: true,callback: {
                    self.shareAction(lois)
                })
            }
            else{
                initTempShare(lois)
            }
        }
        else{
            initTempShare(lois)
        }
    }
    
    func initTempShare(lois:Lois!){
        if(tempShare.getTempShare() != nil && tempShare.getTempShare()?.share_temp_twitterkey != ""){
            
            println("Je partage avec la session twitter d'une demande déjà faite")
            
            TwitterClass.initSession(userInit:false,callback: {
                self.shareAction(lois)
            })
        }
        else{
            println("Je n'ai aucun droit Twitter pour share")
            //Je crée le temps Share pour l'utiliser par la suite en faisant appel à l'autorisation.
            TwitterClass.demandeAuthorization { () -> Void in
                // Je viens d'accepter le share, je modifie mon user en rajoutant le twitter_temp_twitter
                var accessToken = self.swifter.client.credential?.accessToken!
                
                let realm = RLMRealm.defaultRealm()
                realm.beginWriteTransaction()
                
                // SI il est nul on le crée
                if(tempShare.getTempShare() == nil){
                    
                    println("Je rajoute l'object temps share pour twitter & Facebook")
                    
                    let tempShareObject = tempShare()
                    tempShareObject.share_temp_twitterkey = accessToken!.key
                    tempShareObject.share_temp_twittersecret = accessToken!.secret
                    
                    realm.addObject(tempShareObject)
                }
                    // Sinon il existe mais il ne contient pas la clef Twitter
                else{
                    
                    println("Je modifie le temps share pour ajouter la variable Twitter")
                    
                    tempShare.getTempShare()?.share_temp_twitterkey = accessToken!.key
                    tempShare.getTempShare()?.share_temp_twittersecret = accessToken!.secret
                }
                realm.commitWriteTransaction()
                
                self.shareAction(lois)
            }
        }
    }
    
    func shareAction(lois:Lois!){
        
        
        var inputBox = BMInputBox.boxWithStyle(BMInputBox.BMInputBoxStyle.PlainTextInput)
        
        inputBox.cancelButtonText = "Annuler"
        inputBox.submitButtonText = "Twitter !"
        
        inputBox.title = "Partage Twitter"
        //   inputBox.message = "Vous pouvez personnalisez votre message"
        
        inputBox.customiseInputElement = {(element: UITextView) in
            element.text = "Lois utile à connaitre : \(lois.title) \(lois.bitly) \(lois.twitter_hashtag)"
            //                if element.secureTextEntry == true {
            //                    element.placeholder = "Secure placeholder"
            //                }
            return element
        }
        
        inputBox.onSubmit = {(value: AnyObject...) in
            println("submit")
            var twitterText = ""
            for text in value {
                if text is String {
                    twitterText = text as! String
                }
            }
            
            self.swifter.postStatusUpdate(twitterText, inReplyToStatusID: nil, lat: nil, long: nil, placeID: nil, displayCoordinates: nil, trimUser: nil, success: {
                (status: [String : SwifteriOS.JSONValue]?) in
                
                dispatch_async(dispatch_get_main_queue(), {
                    var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
                    TAOverlay.showOverlayWithLabel(nil,options: options)
                })
                
                }, failure: {
                    (error: NSError) in
                    
                    println(error.localizedDescription)
            })
        }
        
        
        inputBox.onCancel = {
            NSLog("Cancelled")
        }
        
        inputBox.show()
    }
    
    func demandeAuthorization(callback: () -> Void){
        let failureHandler: ((NSError) -> Void) = {
            error in

            dispatch_async(dispatch_get_main_queue(), {
                var options = (TAOverlayOptions.OverlayTypeError | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
                TAOverlay.showOverlayWithLabel("Erreur d'authorisation Twitter.",options: options)
            })
            
            //callback()
        }
        
        swifter.authorizeWithCallbackURL(NSURL(string: "fb1399147183714013://success")!, success: {
            accessToken, response in
            
            callback()
            
            },failure: failureHandler
        )
    }
    
    func post(useToken:Bool = false,type:String,params : NSData, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        //        if(!Reachability.isConnectedToNetwork()){
        //            var refreshAlert = UIAlertController(title: "Erreur", message: "Veuillez vérifier votre connection internet.", preferredStyle: UIAlertControllerStyle.Alert)
        //            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        //                println("Ok")
        //            }))
        //
        //            presentViewController(refreshAlert, animated: true, completion: nil)
        //            return
        //        }
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = params
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    
                    //Login ok
                    if(type == "registerTwitterUser"){
                        if let success = parseJSON["sessid"] as? String {
                            //println("Succes:")
                            var data:[String:String] = ["token":parseJSON["token"] as! String]
                            
                            //Création de l'objet user
                            if let user = parseJSON["user"] as? NSDictionary {
                                
                                let realm = RLMRealm.defaultRealm()
                                realm.beginWriteTransaction()
                                UserModel.createOrUpdateInDefaultRealmWithObject(user)
                                println(user)
                                realm.commitWriteTransaction()
                                
                                //Au login du user, je lui met le token.
                                realm.beginWriteTransaction()
                                var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                                connectedUser.token = parseJSON["token"] as! String
                                realm.commitWriteTransaction()
                            }
                            
                            postCompleted(succeeded: true, data: data)
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Une erreur est survenue, verifiez vos champs."])
                        }
                    }
                    return
                }
                else {
                    if(type == "registerTwitterUser"){
                        println("Tentative de connection avec l'utilisateur déjà connecté.")
                        postCompleted(succeeded: true, data: ["msg":"Utilisateur déjà connecté."])
                    }
                    else{
                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr)")
                        postCompleted(succeeded: false, data: ["msg":jsonStr as! String])
                    }
                }
            }
        })
        
        task.resume()
    }
}
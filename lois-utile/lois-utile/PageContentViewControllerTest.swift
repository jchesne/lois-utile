//
//  PageContentViewControllerTest.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 16/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm
import TTTAttributedLabel
import SwiftyJSON


class PageContentViewControllerTest: UIViewController, finishCreationCommentaireDelegate, actionCommentaireDelegate, actionLoisCellDelegate, HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource {


    @IBOutlet weak var horizontalParentContainer: UIView!
        
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var isFavorite: StarButton!
    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var dateAjoutLabel: TTTAttributedLabel!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var nombre_commentaires: UIButton!
    @IBOutlet weak var ressourcesButton: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var pageIndex : Int = 0
    var titleText : String = ""
    
    var detailLoisModel: Lois!
    
    var favorisImage = "favorites"
    var favorisText = "Ajouter en favoris"
    var clickedIndex:NSIndexPath = NSIndexPath(forRow: 5, inSection: 0)
    var scrollPosition:CGPoint!
    
    var ressourcesArray:[String] = []
    
    var etat_vote = VotesModel()
    var etat_favoris = Favoris()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    

    
    var liste_commentaires = LoisComments.allObjects()
    var liste_top_commentaires = LoisComments.allObjects()
    
    
    //var display_commentaires = true
    
    var notificationToken: RLMNotificationToken?
    
    var current_commentaire_action_index:NSIndexPath!
    
    func creationCommentaireFinish(){
        println("Je fais appel à la récupération des commentaires.")
        
        var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
        TAOverlay.showOverlayWithLabel(nil,options: options)
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.getComments()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //        innerView.frame = CGRectMake(0 , 0, scrollView.frame.size.width, tableView.frame.origin.y + tableView.frame.height + 100000)
        //
        //        var frame:CGRect = self.tableView.frame
        //        frame.size.height = tableView.contentSize.height
        //        self.tableView.frame = frame
        //
        //        scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,tableView.frame.height)
        //
        //        innerView.frame = CGRectMake(0 , 0, scrollView.frame.size.width,  100)
        
        //        scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, tableView.frame.origin.y + tableView.frame.height)
        
        //        self.tableView.layoutIfNeeded()
        //
        //        var frame:CGRect = self.tableView.frame
        //        frame.size.height = tableView.contentSize.height
        //        self.tableView.frame = frame
        
        //println("JE DEFINIT LA HAUTEUR")
        self.tableView.layoutIfNeeded()
        self.tableViewHeightConstraint.constant = self.tableView.contentSize.height;
        println("HEIGHT : \(self.tableView.contentSize.height)")
        self.view.layoutIfNeeded()
        


        
    }
    
    // MARK: - HTHorizontalSelectionListDataSource Protocol Methods
    
    func numberOfItemsInSelectionList(selectionList: HTHorizontalSelectionList!) -> Int {
        return 7
    }
    
//    func selectionList(selectionList: HTHorizontalSelectionList!, viewForItemWithIndex index: Int) -> UIView! {
//        return flowers[index]
//    }
    
    func selectionList(selectionList: HTHorizontalSelectionList!, titleForItemWithIndex index: Int) -> String! {
        return "Tag N° \(index)"
    }
    
    // MARK: - HTHorizontalSelectionListDelegate Protocol Methods
    
    func selectionList(selectionList: HTHorizontalSelectionList!, didSelectButtonWithIndex index: Int) {
        // update the view for the corresponding index
        println("PASSE")
        //self.selectedFlowerView.image = self.flowers[index].image
    }

//    
//    - (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
//    // update the view for the corresponding index
//    self.selectedCarLabel.text = self.carMakes[index];
//    }
//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    var selectionList : HTHorizontalSelectionList!
    
    override func viewDidLoad() {
        
        self.selectionList = HTHorizontalSelectionList(frame: CGRectMake(0, 0, self.view.frame.width, 30))
        self.selectionList.delegate = self
        self.selectionList.dataSource = self
        self.selectionList.bottomTrimHidden = true
        
        self.selectionList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyle.None
        
        self.selectionList.buttonInsets = UIEdgeInsetsMake(3, 5, 3, 5);
        
        self.horizontalParentContainer.addSubview(self.selectionList)
        
//        self.horizontalSelectTag.delegate = self;
//        self.horizontalSelectTag.dataSource = self;
//        self.horizontalSelectTag.selectionIndicatorColor = UIColor.redColor()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        
        super.viewDidLoad()
        
        self.liste_commentaires = LoisComments.objectsWhere("nid = '\(detailLoisModel.nid)'").sortedResultsUsingProperty("index", ascending: true)
        let sortProperties = [RLMSortDescriptor(property: "vote_plus", ascending: false), RLMSortDescriptor(property: "date_creation", ascending: false)]
        self.liste_top_commentaires = LoisComments.objectsWhere("nid = '\(detailLoisModel.nid)'").sortedResultsUsingDescriptors(sortProperties)
        
        //Je charge les commentaires de la lois.
        getComments()
        
//        notificationToken = RLMRealm.defaultRealm().addNotificationBlock { note, realm in
//            self.tableView.reloadData()
//        }
        
        
        self.tableView.delaysContentTouches = false
        
        //@TODO: gérer les ressources
        var ressources = 0
        if(detailLoisModel.ressources != ""){
            self.ressourcesArray = detailLoisModel.ressources.componentsSeparatedByString(",")
            ressources = self.ressourcesArray.count
        }
        self.ressourcesButton.setTitle("Ressources (\(ressources))", forState: UIControlState.Normal)
        
        self.etat_vote = VotesModel.lois_etat_vote(self.detailLoisModel)
        self.etat_favoris = Favoris.lois_etat_favoris(self.detailLoisModel)
        
        self.titreLabel.text = detailLoisModel.title
        self.bodyLabel.text = detailLoisModel.body
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        var date_converted = NSDate(timeIntervalSince1970: (detailLoisModel.changed as NSString).doubleValue)
        let date = dateFormatter.stringFromDate(date_converted)
        
        let text = "ajouté le \(date) dans \(detailLoisModel.categorie_name)"
        let attributedString = NSMutableAttributedString(string: text)
        self.dateAjoutLabel.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (NSMutableAttributedString) -> NSMutableAttributedString! in
            var stringRange = (text as NSString).rangeOfString(self.detailLoisModel.categorie_name)
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica-Light", size: 10.0)!, range: NSRange(location: 0,length: (text as NSString).length))
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica", size: 10.0)!, range: stringRange)
            return attributedString
        })
        
        if(self.etat_vote != nil){
            self.topButton.setImage(UIImage(named: "Top-orange"), forState: UIControlState.Normal)
        }else{
            self.topButton.setImage(UIImage(named: "Top-grey"), forState: UIControlState.Normal)
        }
        
        self.topButton.setTitle("\(detailLoisModel.vote_plus)", forState: UIControlState.Normal)
        
        self.nombre_commentaires.setTitle("\(detailLoisModel.comments_count)", forState: UIControlState.Normal)
        
        self.isFavorite.layoutSubviews()
        
        if(self.etat_favoris != nil){
            //On affiche l'étoile orange
            self.isFavorite.isFavorite = true
        }
        else{
            //On affiche l'étoile grise
            self.isFavorite.isFavorite = false
        }
    }
    
    func getComments(){
        self.post(useToken:false,type:"getComments",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/free/contents/getComments") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                println("JE VIENS DE RECUP LES COMMENTAIRES")
                
                //@TODO: Vérifier cette partie!
                self.tableView.reloadData()
                self.tableViewHeightConstraint.constant = self.tableView.contentSize.height;
                self.tableView.updateConstraintsIfNeeded()
                self.tableView.layoutIfNeeded()
                self.tableViewHeightConstraint.constant = self.tableView.contentSize.height;

                self.view.layoutIfNeeded()

                
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        //println(section)
        if(section==0){
            if((Int(liste_top_commentaires.count) > 0) ){
                if((liste_top_commentaires.firstObject() as! LoisComments).vote_plus != 0){
                    if(Int(liste_top_commentaires.count) > 1){
                        if((liste_top_commentaires.objectAtIndex(1) as! LoisComments).vote_plus != 0){
                            return 2
                        }
                        else{
                            return 1
                        }
                    }
                    else{
                        return 1
                    }
                }
                else{
                    return 0
                }
            }
            else{
                return 0
            }
        }
        else{
            if((Int(liste_commentaires.count) > 0)){
                println("Nombre de commentaires \(liste_commentaires.count) pour \(detailLoisModel.title)")
                return Int(liste_commentaires.count)
            }
            else{
                return 0
            }
        }
        
        // Les 7 premiers body + social
        //Header Commentaire top
        //contenu commentaires top
        //Header commentaires
        //contenu commentaires
    }
    
    @IBAction func ajoutLoisFavoris(sender: UIButton) {
         ajout_delete_favoris()
    }

    
    /*
    Fonction de delegate depuis detailLoisBodyCell.
    */
//    func voteLois(){
//        
//    }
    
    @IBAction func clickVoteLois(sender: UIButton) {
        self.scrollPosition = self.tableView.contentOffset
        change_vote(1)
    }
    
    @IBAction func commenterButton(sender: UIButton) {
        self.performSegueWithIdentifier("showCreerCommentaire", sender: self)
    }
    
    @IBAction func showRessources(sender: UIButton) {
        self.performSegueWithIdentifier("showListeRessources", sender: self)
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            //Top commentaires
         if(indexPath.section == 0){
            
            var loisComment = (liste_top_commentaires.objectAtIndex(UInt(indexPath.row)) as! LoisComments)
            
            let comment_top_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCell", forIndexPath: indexPath) as! CommentCell
            comment_top_cell.bodyCommentLabel.text = loisComment.body
            comment_top_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
            
            comment_top_cell.delegate = self
            comment_top_cell.index = indexPath
            
            let URL = NSURL(string:loisComment.avatar)!
            comment_top_cell.avatar.hnk_setImageFromURL(URL)
            
            return comment_top_cell
        }
            //Tous les commentaires
        else{
            var loisComment = (liste_commentaires.objectAtIndex(UInt(indexPath.row)) as! LoisComments)
            

            
            if(loisComment.parent != "0"){
                let comment_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCellChildren", forIndexPath: indexPath) as! CommentCellChildren
                comment_cell.bodyCommentLabel.text = loisComment.body
                comment_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
                
                comment_cell.delegate = self
                comment_cell.index = indexPath
                
                let URL = NSURL(string:loisComment.avatar)!
                comment_cell.avatar.hnk_setImageFromURL(URL)
                
                comment_cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                return comment_cell
            }
            else{
                let comment_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCell", forIndexPath: indexPath) as! CommentCell
                comment_cell.bodyCommentLabel.text = loisComment.body
                comment_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
                
                comment_cell.delegate = self
                comment_cell.index = indexPath
                
                let URL = NSURL(string:loisComment.avatar)!
                comment_cell.avatar.hnk_setImageFromURL(URL)
                
                comment_cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                return comment_cell
            }
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return 44
    }
    
    
    func repondreCommentaire(index:NSIndexPath){
        self.current_commentaire_action_index = index
        performSegueWithIdentifier("showRepondreComment", sender: self)
    }
    
    func notePlusCommentaire(index:NSIndexPath){
        //appel post noter plus
    }
    
    func noteMoinsCommentaire(index:NSIndexPath){
        //appel post noter moins
    }
    
    func signaler(index:NSIndexPath){
        //alert signaler un abus ou autre.
        
        var refreshAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        refreshAlert.addAction(UIAlertAction(title: "Copier le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Copie du commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Partager le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Partager le commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Signaler le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Signaler le commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as! HeaderCell
        
        switch (section) {
        case 0:
            headerCell.headerLabel.text = "Meilleurs commentaires";
            //return sectionHeaderView
        case 1:
            headerCell.headerLabel.text = "\(liste_commentaires.count) commentaires";
            //return sectionHeaderView
        default:
            headerCell.headerLabel.text = "";
        }
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(44)
    }
    
    /*
    Fonction qui gère le click sur les réseaux sociaux et la mise en favoris.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Pour l'instant je ne gère le click que sur la 1ere section.
//        if(indexPath.section == 0){
//            self.scrollPosition = self.tableView.contentOffset
//            
//            self.performSegueWithIdentifier("showListeRessources", sender: self)
//            
//            //            switch indexPath.row {
//            //            case 1:
//            //                shareSocial()
//            //            case 2:
//            //                ajout_delete_favoris()
//            //            case 4:
//            //                change_vote(1)
//            //            case 5:
//            //                change_vote(-1)
//            //            case 6:
//            //
//            //                self.performSegueWithIdentifier("showListeRessources", sender: self)
//            //            default:
//            //                println("Click sur le texte.")
//            //            }
//        }
    }
    
    func shareSocial(){
        var options = (TAOverlayOptions.AllowUserInteraction | TAOverlayOptions.AutoHide | TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Success",options: options)
        
        //@Todo: Afficher une jolie fenêtre pour partager sur les réseaux sociaux, mail, copy url, copy message.
        //FB.shareContent()
        
        //        let failureHandler: ((NSError) -> Void) = {
        //            error in
        //
        //            self.alertWithTitle("Error", message: error.localizedDescription)
        //        }
        
        
        // save session
//                let accessToken = swifter.client.credential.accessToken {
//                    let key = accessToken.key // String
//                    let secret = accessToken.secret // String
//                    // save the key and secret to Keychain
//                }
//        
//                // load session
//                let key = ... // load the key from Keychain
//                let secret = ... // load the secret from Keychain
//                let accessToken = SwifterCredential.OAuthAccessToken(key: key, secret: secret)
//                let credential = SwifterCredential(accessToken: accessToken)
//        
//                let swifter = Swifter(consumerKey: "", consumerSecret: "")
//                swifter.client.credential = credential
//                 Si je n'ai pas de credential ça veut dire que je ne suis pas loggé sur twitter
//                if(swifter.client.credential == nil){
//        
//                }
//                else{
//                    swifter.postStatusUpdate("Hello, world", inReplyToStatusID: nil, lat: nil, long: nil, placeID: nil, displayCoordinates: nil, trimUser: nil, success: {
//                        (status: [String : SwifteriOS.JSONValue]?) in
//        
//                        println("toto")
//        
//                        }, failure: {
//                            (error: NSError) in
//        
//                            println(error.localizedDescription)
//                    })
//                }
        
        
    }
    
    func alertWithTitle(title: String, message: String) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func change_vote(etat:Int){
        // Ici j'appelle le WS qui va créer/supprimer ou modifier le vote.
        
        // Les anonymes et les connecté peuvent voter, mais ça fait appel à 2 WS différents, en fonction du status de connection je fais appel à l'un ou l'autre.
        var useToken = false
        var url = "\(appDelegate.url)/api/v1/fr/free/votingapi/set_votes"
        if((UserModel.getUser()) != nil){
            useToken = true
            url = "\(appDelegate.url)/api/v1/fr/votingapi/set_votes"
        }
        
        self.post(useToken: useToken,type:"setVotes",params: ["entity_id":self.detailLoisModel.nid, "value":String(etat), "type":"node"], url: url) { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    
                    /*
                    Creation / Modification / Suppression du model Vote.
                    
                    Le model Lois a été modifié dans la réponse du WS.
                    */
                    if(self.etat_vote != nil){
                        if(self.etat_vote.etat_vote == etat){
                            //self.etat_vote = recupereVote()
                            realm.deleteObject(self.etat_vote)
                            self.etat_vote = nil
                            println("Je supprime le vote")
                        }
                        else{
                            self.etat_vote.etat_vote = etat
                            println("Je change le vote.")
                        }
                    }
                    else{
                        //On a encore jamais voté, je le crée.
                        var vote = VotesModel()
                        vote.etat_vote = etat
                        vote.id_lois = self.detailLoisModel.nid
                        
                        self.etat_vote = vote
                        
                        realm.addObject(self.etat_vote)
                        println("Je crée le vote.")
                    }
                    
                    if(self.etat_vote != nil){
                        self.topButton.setImage(UIImage(named: "Top-orange"), forState: UIControlState.Normal)
                    }else{
                        self.topButton.setImage(UIImage(named: "Top-grey"), forState: UIControlState.Normal)
                    }
                    
                    // Je met à jour le node avec la valeur des nouveaux votes.
                    
                    realm.commitWriteTransaction()
                    //self.tableView.reloadData()
                    
                    
//                    var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                    
                    
                    // self.tableView.contentOffset = self.scrollPosition
                    
                })
                
            }
            else {
                var alert = UIAlertView(title: "Erreur", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
        
        
    }
    
    
    func ajout_delete_favoris(){
        // Si le favoris est déjà présent je le supprime.
        if(self.etat_favoris != nil){
            
            self.post(useToken: true,type:"deleteFavorites",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/contents/deleteFavorites") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let realm = RLMRealm.defaultRealm()
                        realm.beginWriteTransaction()
                        
                        realm.deleteObject(self.etat_favoris)
                        self.etat_favoris = nil
                        println("Suppression du favoris.")
                        
                        self.isFavorite.isFavorite = false
                        
                        realm.commitWriteTransaction()
                        
//                        var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                       // self.tableView.reloadData()
                    })
                    
                }
                else {
                    var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
            
        }
            // Le favoris n'est pas présent, je le rajoute.
        else{
            //On a encore jamais mis en favoris, je le crée.
            //@TOD0: Vérifier la connection internet.
            self.post(useToken: true,type:"addFavorites",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/contents/addFavorites") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let realm = RLMRealm.defaultRealm()
                        realm.beginWriteTransaction()
                        
                        var favoris = Favoris()
                        var dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "dd/MM/YYYY"
                        //DateFormatter.stringFromDate(NSDate())
                        favoris.date_ajout = data["date_ajout"]!
                        favoris.lois = self.detailLoisModel
                        self.etat_favoris = favoris
                        
                        realm.addObject(self.etat_favoris)
                        
                        realm.commitWriteTransaction()
                        println("Création du favoris.")
                        
                        self.isFavorite.isFavorite = true
                       // self.tableView.reloadData()
                        
//                        var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                    })
                }
                else {
                    var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
        }
    }
    
    func update() {
        //   if(self.scrollPosition != nil){
        //  self.tableView.contentOffset = self.scrollPosition
        // }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showListeRessources" {
            let listeRessourcesTableViewController = segue.destinationViewController as! ListeRessourcesTableViewController
            listeRessourcesTableViewController.ressourcesArray = self.ressourcesArray
        }
        else if(segue.identifier == "showRepondreComment"){
            
            if(UserModel.getUser() == nil){
                var refreshAlert = UIAlertController(title: nil, message: "Vous devez vous connecter pour effectuer cette opération.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Connection", style: .Default, handler: { (action: UIAlertAction!) in
                    println("Je lance la connection")
                    
                    self.performSegueWithIdentifier("showCreationCompteAlert", sender: self)
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    println("J'annule la fenetre")
                }))
                presentViewController(refreshAlert, animated: true, completion: nil)
                return
            }
            
            let createCommentViewController = segue.destinationViewController as! CreationCommentaireViewController
            createCommentViewController.delegate = self
            createCommentViewController.nid = detailLoisModel.nid
            
            
            var loisComment:LoisComments!
            
            // On répond à un commentaire top
            if(self.current_commentaire_action_index.section == 1){
                loisComment = (liste_top_commentaires.objectAtIndex(UInt(self.current_commentaire_action_index.row)) as! LoisComments)
                
            }
                // On répond aux autres commentaires
            else{
                loisComment = (liste_commentaires.objectAtIndex(UInt(self.current_commentaire_action_index.row)) as! LoisComments)
            }
            
            createCommentViewController.pid = loisComment.cid
            createCommentViewController.repondre_a = loisComment.prenom_nom
        }
        else if(segue.identifier == "showCreerCommentaire"){
            
            if(UserModel.getUser() == nil){
                var refreshAlert = UIAlertController(title: nil, message: "Vous devez vous connecter pour effectuer cette opération.", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Connection", style: .Default, handler: { (action: UIAlertAction!) in
                    println("Je lance la connection")
                    
                    self.performSegueWithIdentifier("showCreationCompteAlert", sender: self)
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                    println("J'annule la fenetre")
                }))
                presentViewController(refreshAlert, animated: true, completion: nil)
                return
            }
            
            let createCommentViewController = segue.destinationViewController as! CreationCommentaireViewController
            createCommentViewController.delegate = self
            createCommentViewController.nid = detailLoisModel.nid
        }
    }
    
    
    func post(useToken:Bool = false,type:String,params : Dictionary<String, String>, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        
        if(!Reachability.isConnectedToNetwork()){
            //            var refreshAlert = UIAlertController(title: "Erreur", message: "Veuillez vérifier votre connection internet.", preferredStyle: UIAlertControllerStyle.Alert)
            //            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            //                println("Ok")
            //            }))
            //            presentViewController(refreshAlert, animated: true, completion: nil)
            //            return
            //Je n'ai pas internet, en fonction de l'action souhaité j'affiche ou non l'alerte.
            var array:[String] = ["addFavorites" ,"deleteFavorites" ,"setVotes"]
            var trouve = false
            for value in array{
                if(type == value){
                    trouve = true
                    break
                }
            }
            
            if(trouve){
                NSNotificationCenter.defaultCenter().postNotificationName("noInternetAlert", object: nil)
                return
            }
            else{
                NSNotificationCenter.defaultCenter().postNotificationName("noInternet", object: nil)
                return
            }
        }
        
        if(useToken == true && UserModel.getUser() == nil){
            var refreshAlert = UIAlertController(title: nil, message: "Vous devez vous connecter pour effectuer cette opération.", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Connection", style: .Default, handler: { (action: UIAlertAction!) in
                println("Je lance la connection")
                
                self.performSegueWithIdentifier("showCreationCompteAlert", sender: self)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                println("J'annule la fenetre")
            }))
            presentViewController(refreshAlert, animated: true, completion: nil)
            return
        }
        
        if(type != "getComments"){
            var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel(nil,options: options)
        }
        
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
        
        
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                
                //Je cache le loading
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    TAOverlay.hideOverlay()
                })
                
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Get COmments
                    if(type == "getComments"){
                        if let commentaires = parseJSON["comments"] as? [NSDictionary] {
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                
                            let realm = RLMRealm.defaultRealm()
                            
                            // Ici je crée les catégories / lois en utilisant Realm qui va instancier et mettre à jour automatiquement mes objets.
                            realm.beginWriteTransaction()
                            for commentaire in commentaires {
                                LoisComments.createOrUpdateInDefaultRealmWithObject(commentaire)
                            }
                            
                            //Je met à jour le nombre de commentaires dans mon objet et dans le haut de ma page.
                            self.nombre_commentaires.setTitle("\(commentaires.count)", forState: UIControlState.Normal)
                            
                            
                            self.detailLoisModel.comments_count = commentaires.count
                            
                            realm.commitWriteTransaction()
                                
                                postCompleted(succeeded: true, data:["msg":"Contenu"])
                            })
                            
                            
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Addresse Email déjà utilisée."])
                        }
                    }
                    // Add favoris
                    if(type == "addFavorites"){
                        if let success = parseJSON["success"] as? String {
                            postCompleted(succeeded: true, data:["date_ajout":success])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans l'ajout du favoris."])
                        }
                    }
                    else if(type == "deleteFavorites"){
                        if let success = parseJSON["success"] as? Bool {
                            postCompleted(succeeded: true, data:["":""])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans la suppression du favoris."])
                        }
                    }
                    else if(type == "setVotes"){
                        if let success = parseJSON["success"] as? Bool {
                            let action = parseJSON["action"] as? String
                            
                            if(action == "setvote"){
                                // je dispatche dans le thread principal pour pouvoir mettre à jour mon objet qui a été crée dans le thread principal.
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    let realm = RLMRealm.defaultRealm()
                                    realm.beginWriteTransaction()
                                    //Je set le nombre de vote pour la lois courante.
                                    if let votes = parseJSON["votes"] as? [NSDictionary]{
                                        let json = JSON(parseJSON)
                                        self.detailLoisModel.vote_plus = 0
                                        //  self.detailLoisModel.vote_moins = 0
                                        for (index: String, subJson: JSON) in json["votes"] {
                                            if(subJson["function"] == "option-1"){
                                                var value = subJson["value"].string!
                                                self.detailLoisModel.vote_plus = value.toInt()!
                                                self.topButton.setTitle(value, forState: UIControlState.Normal)
                                            }
                                        }
                                    }
                                    realm.commitWriteTransaction()
                                })
                                
                                postCompleted(succeeded: true, data:["msg":"Setting du vote ok"])
                            }
                            else{
                                postCompleted(succeeded: true, data:["msg":"Suppression du vote ok"])
                            }
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans la suppression du favoris."])
                        }
                    }
                    return
                }
                else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    postCompleted(succeeded: false, data: ["msg":"Erreur."])
                }
            }
        })
        
        task.resume()
    }


}

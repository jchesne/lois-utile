//
//  TagsTableViewCell.swift
//  testTags
//
//  Created by Jérôme Chesne on 26/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class TagsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var tagsView: SKTagView!
    @IBOutlet weak var searchButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.searchButton.layer.borderWidth = 1
        self.searchButton.layer.borderColor = UIColor.lightGrayColor().CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

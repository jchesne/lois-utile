//
//  ViewController.swift
//  Menu
//
//  Created by Mathew Sanders on 9/7/14.
//  Copyright (c) 2014 Mat. All rights reserved.
//

import UIKit
import MessageUI

class shareViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    // create instance of our custom transition manager
    let transitionManager = MenuTransitionManager()
    var loisModel:Lois!
    
    // create references to the items on the storyboard 
    // so that we can animate their properties
    
    @IBOutlet weak var textPostIcon: UIButton!
    @IBOutlet weak var textPostLabel: UILabel!
    
    @IBOutlet weak var photoPostIcon: UIButton!
    @IBOutlet weak var photoPostLabel: UILabel!
    
    @IBOutlet weak var quotePostIcon: UIButton!
    @IBOutlet weak var quotePostLabel: UILabel!
    
    @IBOutlet weak var linkPostIcon: UIButton!
    @IBOutlet weak var linkPostLabel: UILabel!
    
    @IBOutlet weak var chatPostIcon: UIButton!
    @IBOutlet weak var chatPostLabel: UILabel!
    
    @IBOutlet weak var audioPostIcon: UIButton!
    @IBOutlet weak var audioPostLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        self.transitioningDelegate = self.transitionManager

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func shareFacebook(sender: UIButton) {
        FB.shareContent(self.loisModel)
    }

    @IBAction func shareTwitter(sender: UIButton) {
        TwitterClass.postToTwitter(self.loisModel)
    }
    
    @IBAction func shareMail(sender: UIButton) {
        var emailTitle = loisModel.title
        var messageBody = "\(loisModel.body)\n\nVia l'application Lois Utiles."

        
        var mc:MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)

        //mc.navigationBar.translucent = false
        mc.navigationBar.tintColor = UIColor.whiteColor()
        self.presentViewController(mc, animated: true) { () -> Void in
            UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        }
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MFMailComposeResultSaved.value:
            println("Mail saved")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MFMailComposeResultSent.value:
            println("Mail sent")
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
                TAOverlay.showOverlayWithLabel(nil,options: options)
            })
        case MFMailComposeResultFailed.value:
            println("Mail sent failure: %@", [error.localizedDescription])
            self.dismissViewControllerAnimated(true, completion: nil)
        default:
            break
        }
        
    }
    
    @IBAction func shareMessage(sender: UIButton) {
        var messageVC = MFMessageComposeViewController()
        //@TODO: rensseigner l'url définitive.
        messageVC.body = "\(loisModel.title)\n\(loisModel.body)\n\nVia l'application Lois Utiles.";
        messageVC.messageComposeDelegate = self;
        
        self.presentViewController(messageVC, animated: false, completion: nil)
    }
    
    @IBAction func copyText(sender: UIButton) {
        let pb: UIPasteboard = UIPasteboard.generalPasteboard();
        pb.string = "\(loisModel.title)\n\(loisModel.body)\n\nVia l'application Lois Utiles."
        var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
        TAOverlay.showOverlayWithLabel(nil,options: options)
    }
    
    @IBAction func copyLink(sender: UIButton) {
        let pb: UIPasteboard = UIPasteboard.generalPasteboard();
        pb.string = loisModel.link
        var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
        TAOverlay.showOverlayWithLabel(nil,options: options)
    }
    
    
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        switch (result.value) {
        case MessageComposeResultCancelled.value:
            println("Message was cancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultFailed.value:
            println("Message failed")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultSent.value:
            
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
                TAOverlay.showOverlayWithLabel(nil,options: options)
            })
        default:
            break;
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == nil {
                NSNotificationCenter.defaultCenter().postNotificationName("cacheShareScreen", object: nil)
        }
    }

    
}


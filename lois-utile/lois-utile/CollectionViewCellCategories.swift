import UIKit

class CollectionViewCellCategories: UICollectionViewCell {
    @IBOutlet weak var nom: UILabel!
    @IBOutlet weak var count: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}

//
//  NotificationPersonnalisationViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 25/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class NotificationPersonnalisationViewController: UITableViewController {

    var categories = Categorie.allObjects()
    
    let dataHeader = [
        0:"Notification ajout de lois",
        1:"Notification modification de lois"
    ]
    
    var displayAjout:Bool = true
    var displayModification:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.dataHeader.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if(section == 0){
            if(self.displayAjout){
                return Int(self.categories.count)
            }
            else{
               return 0
            }
        }
        else{
            if(self.displayModification){
                return Int(self.categories.count)
            }
            else{
                return 0
            }
        }
        
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var categorie = categories[UInt(indexPath.row)] as! Categorie
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationTableViewCell", forIndexPath: indexPath) as! NotificationTableViewCell
        cell.notificationLabel.text = categorie.name
        return cell
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("NotificationHeaderTableViewCell") as! NotificationHeaderTableViewCell
        headerCell.notificationLabel.text = dataHeader[section]
        headerCell.notificationSwitch.tag = section
        if(section == 0){
            headerCell.notificationSwitch.on = self.displayAjout
        }
        else{
            headerCell.notificationSwitch.on = self.displayModification
        }
        
        headerCell.notificationSwitch.addTarget(self, action:"test:", forControlEvents: UIControlEvents.ValueChanged)
        return headerCell
    }
    
    func test(sender:UISwitch!){
        // Je modifie la notification lors de la creation d'une lois
        if(sender.tag == 0){
            if(sender.on){
                // j'affiche tous
                println("J'affiche tous ajout")
                self.displayAjout = true
            }
            else{
                // je cache tous
                println("Je cache tous ajout")
                self.displayAjout = false
            }
        }
        // Je modifie la notification lors de la modification d'une lois
        else{
            if(sender.on){
                // j'affiche tous
                println("J'affiche tous modification")
                self.displayModification = true
            }
            else{
                // je cache tous
                println("Je cache tous modification")
                self.displayModification = false
            }
        }
        
        self.tableView.reloadData()
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */


}

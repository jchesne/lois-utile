//
//  ListeLoisCategoriesControllerTableViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 16/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

class ListeLoisCategoriesControllerTableViewController: UITableViewController {
   
    
    var ListeLoisParCategorie = RLMArray(objectClassName: Lois.className())
    @IBOutlet var tableViewController: UITableView!

    
    override func viewDidLoad() {
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 128.0
        
        super.viewDidLoad()

        tableView.registerNib(UINib(nibName: "ListLoisCell", bundle: nil), forCellReuseIdentifier: "ListLoisCell")        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return Int(ListeLoisParCategorie.count)
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var lois = ListeLoisParCategorie[UInt(indexPath.row)] as! Lois
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ListLoisCell", forIndexPath: indexPath) as! ListLoisCell
        cell.body.text = lois.body
        cell.titre.text = lois.title
        cell.nombre_commentaires.text = "\(lois.comments_count)"
        cell.vote_plus.setTitle("\(lois.vote_plus)", forState: UIControlState.Normal)
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        var date_converted = NSDate(timeIntervalSince1970: (lois.changed as NSString).doubleValue)
        let date = dateFormatter.stringFromDate(date_converted)
        
        let text = "ajouté le \(date) dans \(lois.categorie_name)"
    
        let attributedString = NSMutableAttributedString(string: text)
        cell.date_ajout.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (NSMutableAttributedString) -> NSMutableAttributedString! in
            var stringRange = (text as NSString).rangeOfString(lois.categorie_name)
            attributedString.addAttribute(kCTFontAttributeName as String, value: UIFont(name: "Helvetica-Light", size: 10.0)!, range: NSRange(location: 0,length: (text as NSString).length))
            attributedString.addAttribute(kCTFontAttributeName as String, value: UIFont(name: "Helvetica", size: 10.0)!, range: stringRange)
            return attributedString
        })
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        cell.layoutIfNeeded()
        return cell
    }


    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ListeLoisToDetail", sender: tableView)
    }


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ListeLoisToDetail" {
            let indexPath: AnyObject? = self.tableViewController.indexPathForSelectedRow()

            let detailLoisViewController = segue.destinationViewController as! UILoisPageViewController
            detailLoisViewController.currentIndex = indexPath!.row!
            
            let predicate = NSPredicate(format: "categorie_name = %@", (self.ListeLoisParCategorie.firstObject() as! Lois).categorie_name)
            
            var listeLoisParCategorieObject = Lois.objectsWithPredicate(predicate)
            detailLoisViewController.ListeLoisParCategorie = listeLoisParCategorieObject
        }
    }
}
//
//  CustomTabBarController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 13/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import Foundation

// Add anywhere in your app
extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext() as CGContextRef
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, kCGBlendModeNormal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        tintColor.setFill()
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}



class CustomTabBarController: UITabBarController {
    
    
    @IBOutlet weak var customisedTabBar: UITabBar!
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func viewDidLoad() {
        for item in self.tabBar.items as! [UITabBarItem] {
            if let image = item.image {
                item.image = image.imageWithColor(appDelegate.gris_button_tabbar_inactif).imageWithRenderingMode(.AlwaysOriginal)
            }
            
            var topBorder:CALayer = CALayer()
            topBorder.frame = CGRectMake(0, 0, self.view.frame.size.width, 0.5)
            topBorder.backgroundColor = appDelegate.orange.CGColor
            
            self.customisedTabBar.layer.addSublayer(topBorder)
            
            self.tabBar.clipsToBounds = true
        }
        
        
        
    }
}

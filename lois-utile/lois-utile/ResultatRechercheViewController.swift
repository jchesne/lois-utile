//
//  ResultatRechercheViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 31/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class ResultatRechercheViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, actionLoisCellDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var data:[Lois] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250.0
        // Do any additional setup after loading the view.
        
        tableView.registerNib(UINib(nibName: "ListLoisCell", bundle: nil), forCellReuseIdentifier: "ListLoisCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->
        Int {
            if(Int(data.count) > 0){
                self.tableView.backgroundView = nil;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
                return Int(data.count)
            }
            else{
                var message:UILabel = UILabel()
                message.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
                
                message.text = "Il n'y a aucune lois. oups...";
                message.textColor = UIColor.blackColor()
                message.numberOfLines = 0;
                message.textAlignment = NSTextAlignment.Center;
                message.font = UIFont(name: "Helvetica-Light", size: 14.0)
                message.sizeToFit()
                
                self.tableView.backgroundView = message;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
                
                return 0
            }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListLoisCell", forIndexPath: indexPath) as! ListLoisCell
        //cell.backgroundColor = UIColor.blackColor()
        
        
        //var lois = fetchedResultsControllerLois.objectAtIndexPath(indexPath) as LoisModel
        var lois = data[Int(indexPath.row)]
        cell.titre.text = lois.title
        //   cell.categorie.text = lois.categorie_name
        cell.body.text = lois.body
        cell.vote_plus.setTitle("\(lois.vote_plus)", forState: UIControlState.Normal)
        cell.nombre_commentaires.text = "\(lois.comments_count)"
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        var date_converted = NSDate(timeIntervalSince1970: (lois.changed as NSString).doubleValue)
        let date = dateFormatter.stringFromDate(date_converted)
        
        let text = "ajouté le \(date) dans \(lois.categorie_name)"
        let attributedString = NSMutableAttributedString(string: text)
        cell.date_ajout.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (NSMutableAttributedString) -> NSMutableAttributedString! in
            var stringRange = (text as NSString).rangeOfString(lois.categorie_name)
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica-Light", size: 10.0)!, range: NSRange(location: 0,length: (text as NSString).length))
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica", size: 10.0)!, range: stringRange)
            return attributedString
        })

        cell.delegate = self
        cell.layoutIfNeeded()
        cell.index = indexPath
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

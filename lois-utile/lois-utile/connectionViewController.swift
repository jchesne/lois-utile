//
//  connectionViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 03/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

extension String {
    func isEmail() -> Bool {
        let regex = NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .CaseInsensitive, error: nil)
        return regex?.firstMatchInString(self, options: nil, range: NSMakeRange(0, count(self))) != nil
    }
}

class connectionViewController: UIViewController, UITextFieldDelegate{
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var emailLabel: FloatLabelTextField!
    @IBOutlet weak var passwordLabel: FloatLabelTextField!
    @IBOutlet weak var loginButton: UIButton!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func finishInitUser(){
        dispatch_async(dispatch_get_main_queue(), {
            self.cacheMe()
        })
        
        //        let userInfo:Dictionary<String,String!> = notification.userInfo as Dictionary<String,String!>
        //        let messageString = userInfo["message"]
        //
        //        var refreshAlert = UIAlertController(title: "Erreur", message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
        //        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        //            println("Ok")
        //        }))
        //        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        self.emailLabel.delegate = self
        self.passwordLabel.delegate = self
        
        self.emailLabel.returnKeyType = UIReturnKeyType.Next
        self.passwordLabel.returnKeyType = UIReturnKeyType.Done


        disableLoginButton()
        
        self.emailLabel.addTarget(self, action: "emailChange:", forControlEvents: UIControlEvents.EditingChanged)
        
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    func emailChange(textField: UITextField){
       checkEnableButton()
    }
    
    func checkEnableButton(){
        if(self.emailLabel.text.isEmail() && self.passwordLabel != ""){
            enableLoginButton()
        }
        else{
            disableLoginButton()
        }
    }
    
    func enableLoginButton(){
        self.loginButton.enabled = true
        self.loginButton.backgroundColor = UIColor.greenColor()
    }
    func disableLoginButton(){
        self.loginButton.enabled = false
        self.loginButton.backgroundColor = UIColor.whiteColor()
    }
    
    @IBAction func loginButonClicked(sender: UIButton) {
        login()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.emailLabel) {
            self.passwordLabel.becomeFirstResponder()
        }
        else if (textField == self.passwordLabel) {
            textField.resignFirstResponder()
            login()
        }
        return true;
    }
    
    func login(){
        
        var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Connection en cours...",options: options)
        
        var email = emailLabel.text
        var password = passwordLabel.text
        datasSingleton.sharedInstance.login_user(email,password: password)
    }

    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showError:",
            name: "showError", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "PreviousPage",
            name: "finishInitUser", object: nil)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "showError", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "finishInitUser", object: nil)
    }
    
    @IBAction func cacheMe(){
        PreviousPage()
    }
    
    
    func PreviousPage(){
        dispatch_async(dispatch_get_main_queue(), {
            TAOverlay.hideOverlay()
        })
        
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("backToPreviousPage", object: nil)
        })
    }
    
//    @IBAction func unwindToLoisDetailScreen(segue:UIStoryboardSegue) {
//        
//    }
    
    @IBAction func facebookLogin(){
        FB.login({ self.handleLogin($0) });
    }
    
    func handleLogin(){
        FB.getInfo(loginAfterGettingInfo: true, useToken: false, displayLoader: true)
    }
    
    func showError(notification:NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let messageString = userInfo["message"]

        var refreshAlert = UIAlertController(title: "Erreur", message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                            println("Ok")
                        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func connectTwitter(sender: AnyObject) {
        TwitterClass.demandeAuthorization { () -> Void in
            TwitterClass.login(useToken: false,displayLoader: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func creerUserFacebook(){

    }
}

//
//  PasswordRecoveryViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 17/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class PasswordRecoveryViewController: UIViewController {

    @IBOutlet weak var email: FloatLabelTextField!
    
    let dataSingleton = datasSingleton.sharedInstance
    
    @IBAction func passwordRecovery(sender: UIButton) {
        dataSingleton.passwordRecovery(self.email.text,callback: {
            dispatch_async(dispatch_get_main_queue(), {
                 println("Success back")
                self.dismissViewControllerAnimated(true, completion: nil)
            })
        })
       
    }
}

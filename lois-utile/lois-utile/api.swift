//
//  API.swift
//
//  Created by Taro Minowa on 6/10/14.
//  Copyright (c) 2014 Higepon Taro Minowa. All rights reserved.
//

import Foundation
import UIKit
import Realm

typealias JSONDictionary = Dictionary<String, AnyObject>
typealias JSONArray = Array<AnyObject>


class Categorie: RLMObject {
    dynamic var tid = ""
    dynamic var name = ""
    dynamic var lois = RLMArray(objectClassName: Lois.className())
    override class func primaryKey() -> String {
        return "tid"
    }
}

class Lois: RLMObject {
    dynamic var nid = ""
    dynamic var title = ""
    dynamic var title_recherche = ""
    dynamic var tags = ""
    dynamic var tags_recherche = ""
    dynamic var body = ""
    dynamic var link = ""
    dynamic var bitly = ""
    dynamic var twitter_hashtag = ""
    dynamic var changed = ""
    dynamic var categorie_name = ""
    //dynamic var vote_moins = 0
    dynamic var vote_plus = 0
    dynamic var comments_count = 0
    dynamic var ressources = ""
    

    override class func primaryKey() -> String {
        return "nid"
    }
    
    class func getMotsClefs()->[String:String]{
        var tags:Dictionary<String,String> = Dictionary<String,String>()
        for lois in Lois.allObjects() {
            if((lois as! Lois).tags != ""){
                var tagsArray = (lois as! Lois).tags.componentsSeparatedByString(" ")
                for tag in tagsArray{
                    tags[tag] = tag
                }
            }
        }

        return tags
    }
}

class Recherche: RLMObject {
    dynamic var date_ajout = ""
    dynamic var tags = ""
    dynamic var categories = ""
    dynamic var count = 0
}

class Favoris: RLMObject {
    dynamic var date_ajout = ""
    dynamic var lois = Lois()
    
    class func lois_etat_favoris(detailLoisModel:Lois)->Favoris?{
        if Favoris.objectsWhere("lois.nid = '\(detailLoisModel.nid)'").count > 0 {
            return Favoris.objectsWhere("lois.nid = '\(detailLoisModel.nid)'").firstObject() as? Favoris
        }
        else{
            return nil
        }
    }
}

class VotesModel: RLMObject {

    dynamic var id_lois = ""
    dynamic var etat_vote = 0
    
    override class func primaryKey() -> String {
        return "id_lois"
    }
    
    class func lois_etat_vote(detailLoisModel:Lois)->VotesModel?{
        if VotesModel.objectsWhere("id_lois = '\(detailLoisModel.nid)'").count > 0 {
            return VotesModel.objectsWhere("id_lois = '\(detailLoisModel.nid)'").firstObject() as? VotesModel
        }
        else{
            return nil
        }
    }
}

class UserModel: RLMObject {
    dynamic var uid = ""
    dynamic var nom = ""
    dynamic var prenom = ""
    dynamic var picture_url = ""
    dynamic var password = ""
    dynamic var mail = ""
    dynamic var type = ""
    dynamic var token = ""
    
    
    dynamic var facebooktoken = ""
    dynamic var twittertoken = ""
    
    dynamic var twitterkey = ""
    dynamic var twittersecret = ""
    dynamic var twitterUID = ""
    dynamic var twitterScreenName = ""
    
    override class func primaryKey() -> String {
        return "uid"
    }
    
    class func getUser()->UserModel?{
        return UserModel.allObjects().firstObject() as? UserModel
    }
}

class tempShare: RLMObject{
    /*
    Si l'utilisateur ne se connecte pas via twitter mais que je veux share via twitter
    Je stoke le twitterkey et secret lorsque je fais la demande de share et que je récupère l'authorisation, ça évite d'avoir à demander sans interferer dans le système de connection
    
    Lorsque je veux share via Twitter :
    -Je regarde si l'user est connecté via Twitter
    -Je regarde si il a déjà accepté le Twitter share.
    */
    dynamic var share_temp_twitterkey = ""
    dynamic var share_temp_twittersecret = ""
    
    class func getTempShare()->tempShare?{
        return tempShare.allObjects().firstObject() as? tempShare
    }
}

class UserComments: RLMObject {
    dynamic var cid = ""
    dynamic var date_creation = ""
    dynamic var title_lois = ""
    dynamic var nid = ""
    dynamic var body = ""
    
    override class func primaryKey() -> String {
        return "cid"
    }
}

class LoisComments: RLMObject {
    dynamic var cid = ""
    dynamic var date_creation = ""
    dynamic var nid = ""
    dynamic var body = ""
    dynamic var vote_moins = 0
    dynamic var vote_plus = 0
    dynamic var parent = ""
    dynamic var index = 0
    
    // Données liés à l'auteur du commentaire.
    dynamic var avatar = ""
    dynamic var prenom_nom = ""
    
    
    override class func primaryKey() -> String {
        return "cid"
    }
}

class VotesCommentsModel: RLMObject {
    
    dynamic var id_commentaire = ""
    dynamic var etat_vote = 0
    
    override class func primaryKey() -> String {
        return "id_commentaire"
    }
}

class deviceModel: RLMObject {
    
    dynamic var token = ""
    
    override class func primaryKey() -> String {
        return "token"
    }
    
    class func geDevice()->deviceModel?{
        return deviceModel.allObjects().firstObject() as? deviceModel
    }
}

class API: NSObject, NSURLConnectionDataDelegate {
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    
    func getContents(){
        self.post(type:"getContents",params: ["":""], url: "\(appDelegate.url)/api/v1/fr/free/contents/getAll") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                //self.token = data["token"]!
               // println("success")
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                alert.title = "Erreur."
                alert.message = data["msg"]
                // Move to the UI thread
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    func post(useToken:Bool = false,type:String,params : Dictionary<String, String>, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        
        
        var err: NSError?
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
          //  println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
       //     println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it

                        if let categories = parseJSON["categories"] as? [NSDictionary] {
                            
                            let realm = RLMRealm.defaultRealm()
                            
                            // Ici je crée les catégories / lois en utilisant Realm qui va instancier et mettre à jour automatiquement mes objets.
                            realm.beginWriteTransaction()
                            for categorie in categories {
                                Categorie.createOrUpdateInDefaultRealmWithObject(categorie)
                            }
                            realm.commitWriteTransaction()
                            
                            println("Récupération du contenu OK")

                            
                            postCompleted(succeeded: true, data:["msg":"Contenu"])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Addresse Email déjà utilisée."])
                        }

           
                    return
                }
                else {
                        // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr)")
                        postCompleted(succeeded: false, data: ["msg":"Erreur."])
                }
            }
        })
        
        task.resume()
    }
}
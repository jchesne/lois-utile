//
//  CommentCell.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 06/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import Haneke

protocol actionCommentaireDelegate{
    func repondreCommentaire(index:NSIndexPath)
    func notePlusCommentaire(index:NSIndexPath)
    //func noteMoinsCommentaire(index:NSIndexPath)
    func signaler(index:NSIndexPath)
}

class CommentCell: UITableViewCell {

    @IBOutlet weak var auteurLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bodyCommentLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    var delegate:actionCommentaireDelegate?
    
    var index:NSIndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        var bottomBorder = CALayer()
//        bottomBorder.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, 1.0);
//        bottomBorder.backgroundColor = UIColor.lightGrayColor().CGColor
//        self.layer.addSublayer(bottomBorder)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func repondreCommentaire(sender: AnyObject) {
        self.delegate?.repondreCommentaire(index)
    }

    @IBAction func notePlus(sender: UIButton) {
        self.delegate?.notePlusCommentaire(index)
    }

    
//    @IBAction func noteMoins(sender: UIButton) {
//        self.delegate?.noteMoinsCommentaire(index)
//    }
    
    
    @IBAction func signalerCommentaire(sender: UIButton) {
        self.delegate?.signaler(index)
    }
    
    override func prepareForReuse() {
        avatar.hnk_cancelSetImage()
        avatar.image = nil
    }
}

//
//  TagRechercheViewController.swift
//  testTags
//
//  Created by Jérôme Chesne on 26/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

extension RLMResults {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for result in self {
            if let result = result as? T {
                array.append(result)
            }
        }
        return array
    }
}

class TagRechercheViewController: UIViewController, KSTokenViewDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tagSearch: KSTokenView!
    
    //Source de la liste des catégories.
    let categorie_source = Categorie.allObjects()
    //Source de la liste des mots clefs.
    var mots_clefs_source = Lois.getMotsClefs()
    
    var filterCategorie:Dictionary<String,String> = Dictionary<String,String>()
    var filterMotsClefs:[String] = []
    
    var resultArray:[Lois] = []
    
    var historiqueRecherche = Recherche.allObjects().sortedResultsUsingProperty("date_ajout", ascending: false)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tagSearch.delegate = self
        tagSearch.promptText = ""
        tagSearch.placeholder = "Mots clefs"
        tagSearch.descriptionText = "Mots clefs"
        tagSearch.maxTokenLimit = 5
        tagSearch.style = .Rounded
        tagSearch.direction = .Horizontal
        
        for categorie in categorie_source{
            filterCategorie[(categorie as! Categorie).name] = (categorie as! Categorie).name
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
       
    }
    
    override func viewWillAppear(animated: Bool) {
         self.tableView.reloadData()
    }
    
    
    @IBAction func Rechercher(sender: UIButton) {
        
        if(self.filterMotsClefs.count == 0){
            var options = (TAOverlayOptions.OverlayTypeInfo | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
            TAOverlay.showOverlayWithLabel("Votre recherche est vide.",options: options)
            return
        }
        
        println(self.filterMotsClefs)
        println(filterCategorie)
        
        println(filterCategorie.keys.array)
        
        let predicate = NSPredicate(format: "categorie_name IN %@", filterCategorie.keys.array)
        
        // Récuperation des lois dans les catégories selectionnées.
        var lois = Lois.objectsWithPredicate(predicate)
        //je récupère la liste des ID.
        //A afficher en 1er
        
        //Je boucle parmis les mots clefs pour faire x predicates.
        //Je regarde si dans les lois, certaines on dans le titre les mots clefs.
        //Les lois qui ressortent doivent être en 1er
        
        //dans la liste complète d'ID je supprime la liste d'id des titres.
        //dans la dernièe liste d'id je récupère celles qui ont les tags
        
        //je supprime dans la liste les id titre + id tags
        //à afficher en 2nd
        
        //j'affiche le reste des ids.
        
        var predicateTitleArray:[NSPredicate] = []
        var predicateTagsArray:[NSPredicate] = []
        for motsclefs in self.filterMotsClefs{
            let predicateTitre = NSPredicate(format: "title_recherche CONTAINS[c] %@", motsclefs)
            let predicateTags = NSPredicate(format: "tags_recherche CONTAINS[c] %@", motsclefs)
            predicateTitleArray.append(predicateTitre)
            predicateTagsArray.append(predicateTags)
        }
        var predicatesTitre = NSCompoundPredicate(type: NSCompoundPredicateType.OrPredicateType, subpredicates: predicateTitleArray)
        var loisAvecTitreTags = lois.objectsWithPredicate(predicatesTitre)
        
        //Je boucle parmis les loisAvecTitreTags pour récupérer uniquement les ID
        var loisID:[String] = []
        for selectedTitle in loisAvecTitreTags{
            loisID.append((selectedTitle as! Lois).nid)
        }
        
        //Ici peut-être chercher dans le body de la lois, pour des raisons de perfs je verrais après.
        
        let predicateTags = NSPredicate(format: "NOT (nid IN %@)", loisID)
        predicateTagsArray.append(predicateTags)
        
        var predicatesTags = NSCompoundPredicate(type: NSCompoundPredicateType.AndPredicateType, subpredicates: predicateTagsArray)
        var loisAvecTagsTags = lois.objectsWithPredicate(predicatesTags)
        

//        for selectedTags in loisAvecTagsTags{
//            loisID.append((selectedTags as Lois).nid)
//        }
        

//        let predicateFinal = NSPredicate(format: "NOT (nid IN %@)", loisID)!
//        var resultatFinal = lois.objectsWithPredicate(predicateFinal) // Celui du début - les 2 autres

        

        
        println(lois)//toutes les lois des categories selectionnées (la plus complète et large)
        println("---------------------------------------------------------------------------------------------------------")
        println(loisAvecTitreTags)//Toutes les lois des categories selectionnées + mot clefs dans le titre
        println("---------------------------------------------------------------------------------------------------------")
        println(loisAvecTagsTags)//Toutes les lois des categories selectionnées + mot clefs dans les tags
        println("---------------------------------------------------------------------------------------------------------")
       // println(resultatFinal)//Toutes les lois des categories selectionnées sans les 2 autres.
       // println("---------------------------------------------------------------------------------------------------------")
        
        //Je convertis les résultats de recherche en tableau pour après pouvoir les aditionner.
        var premier = loisAvecTitreTags.toArray(Lois.self)
        var deuxieme = loisAvecTagsTags.toArray(Lois.self)
       // var troisieme = resultatFinal.toArray(Lois.self)

         self.resultArray =  premier + deuxieme
        
//        for unique in all {
//            println(unique.title)
//        }
        //TODO: Finir
        if(resultArray.count == 0){
            var options = (TAOverlayOptions.OverlayTypeInfo | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
            TAOverlay.showOverlayWithLabel("Aucun résultat trouvé.",options: options)
        }
        else{
            //Je sauvegarde le résultat dans la base locale.
            //Je rajoute si le nombre est inférieur à 15.
            //Sinon je modifie la plus vieille recherche
            
            let realm = RLMRealm.defaultRealm()
            realm.beginWriteTransaction()
            
            if(self.historiqueRecherche.count == 15){
                var last = historiqueRecherche.lastObject() as! Recherche
                
                last.date_ajout = "\(NSDate().timeIntervalSince1970)"
                last.categories = join(" ", filterCategorie.keys)
                last.tags = join(" ", self.filterMotsClefs)
                last.count = Int(self.resultArray.count)
            }
            else{
                var recherche = Recherche()
                recherche.date_ajout = "\(NSDate().timeIntervalSince1970)"
                recherche.categories = join(" ", filterCategorie.keys)
                recherche.tags = join(" ", self.filterMotsClefs)
                recherche.count = Int(self.resultArray.count)
                realm.addObject(recherche)
            }
            
            realm.commitWriteTransaction()
            
            self.performSegueWithIdentifier("showResultatRecherche", sender: self)
        }
        
        
        //println(all.count)
    }

    
    /*
    Fonction qui gère l'affichage de la recherche en haut avec les mots clefs issues des mots clefs des lois.
    */
    func tokenView(token: KSTokenView, performSearchWithString string: String, completion: ((results: Array<AnyObject>) -> Void)?) {
        var data: Array<String> = []
        for (myKey,myValue) in mots_clefs_source {
            if myValue.lowercaseString.rangeOfString(string.lowercaseString) != nil {
                data.append(myValue)
            }
        }
        completion!(results: data)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 2
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        else{
            return 44
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->
        Int {
            if(section == 0){
               return 1
            }
            else{
                return Int(historiqueRecherche.count)
            }
            
    }
    
    /*
    Fonction qui gère l'ajout/suppression des categories de recherche
    */
    func removeAddCategorieFilter(obj:UInt){
        var value = (categorie_source[obj] as! Categorie).name
        if((self.filterCategorie.indexForKey(value)) != nil){
            self.filterCategorie.removeValueForKey(value)
        }
        else{
            self.filterCategorie[value] = value
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("TagsTableViewCell", forIndexPath: indexPath) as! TagsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            configureCell(cell,index: indexPath)
            return cell
        }
        else{
            let cell = UITableViewCell()
            var currentRecherche = self.historiqueRecherche[UInt(indexPath.row)] as! Recherche
            cell.textLabel?.text = "Recherche recente \(indexPath.row + 1)" + currentRecherche.tags
            return cell
        }
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("headerCellParametres") as! headerCellParametres
        headerCell.headerLabel.text = "Historique des recherches"
        return headerCell
    }
    
    func configureCell(cell:TagsTableViewCell, index:NSIndexPath){
        cell.tagsView.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.size.width
        cell.backgroundColor = UIColor.grayColor()
        cell.tagsView.padding    = UIEdgeInsetsMake(12, 12, 12, 12);
        cell.tagsView.insets    = 15;
        cell.tagsView.lineSpace = 10;
        cell.tagsView.removeAllTags()
        var weakView:SKTagView = cell.tagsView
        cell.tagsView.didClickTagAtIndex = {
            obj in
            weakView.editTagAtIndex(obj)
            self.removeAddCategorieFilter(obj)
        }
        
        for (index, element) in enumerate(self.categorie_source) {
            var tag:SKTag = SKTag()
            var name = categorie_source[UInt(index)].name
            tag.isActive = (self.filterCategorie.indexForKey(name) != nil) ? true : false
            tag.text = categorie_source[UInt(index)].name
            tag.textColor = UIColor(red: 119/255, green: 138/255, blue: 140/255, alpha: 1)
            tag.fontSize = 15;
            tag.padding = UIEdgeInsetsMake(11, 11, 11, 11);
            tag.bgColor = UIColor.whiteColor()
            tag.borderWidth = 1.0
            tag.borderColor = UIColor.grayColor()
            tag.cornerRadius = 8;
            cell.tagsView.addTag(tag)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0){
            
           let cell = tableView.dequeueReusableCellWithIdentifier("TagsTableViewCell") as! TagsTableViewCell
            configureCell(cell,index: indexPath)
            return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height + 1
        }
        else{
            return 44
        }
    }
    
    func tokenView(token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        return object as! String
    }
    
    func tokenViewDidEndEditing(tokenView: KSTokenView) {
        var tokens = tokenView.tokens()!
        self.filterMotsClefs.removeAll(keepCapacity: false)
        for test in tokens{
            self.filterMotsClefs.append(test.title)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showResultatRecherche" {

            let detailLoisViewController = segue.destinationViewController as! ResultatRechercheViewController
            detailLoisViewController.data = self.resultArray
            //            let detailLoisViewController = segue.destinationViewController as PageContentViewController
            //            let indexPath: AnyObject = tableView.indexPathForSelectedRow()!
            //            let selectedLois:Lois = liste_Lois[UInt(indexPath.row)] as Lois
            //            detailLoisViewController.detailLoisModel = selectedLois
        }
    }
    
}

//
//  UILoisPageViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 21/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

class UILoisPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {

    var currentIndex : Int = 0
    var ListeLoisParCategorie = Lois.allObjects()
    var next = UIBarButtonItem()
    var previous = UIBarButtonItem()
    
    var currentLois:Lois!
    
    var displayFavorites = false
    var arrayOfRecords = RLMArray(objectClassName: "Lois")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.dataSource = self;
        self.delegate = self;
        
        
        var indexPath = NSIndexPath(forRow: self.currentIndex, inSection: 0)
        
        if(self.displayFavorites){
            self.currentLois = self.arrayOfRecords[UInt(indexPath.row)] as! Lois
        }
        else{
            self.currentLois = self.ListeLoisParCategorie[UInt(indexPath.row)] as! Lois
        }
        
        
        let startingViewController: PageContentViewControllerTest = self.viewControllerAtIndex(currentIndex)!
        let viewControllers: NSArray = [startingViewController]
        self.setViewControllers(viewControllers as [AnyObject], direction: .Forward, animated: false, completion: nil)
        
        self.didMoveToParentViewController(self)
        
        self.title = self.currentLois.title
    }

    
    @IBAction func unwindToLoisDetailScreen(segue:UIStoryboardSegue) {
    }

    /*
    Fonction apellé quand je ferme la modale de share.
    */
    @IBAction func unwindToMainViewController (sender: UIStoryboardSegue){
        // bug? exit segue doesn't dismiss so we do it manually...
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func shareButtonCLicked(sender: UIButton) {
        self.performSegueWithIdentifier("showShareDialog", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
       // println("PASSE decremente")
        var index = (viewController as! PageContentViewControllerTest).pageIndex
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        
        index--
        
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
       // println("PASSE incremente")
        
        var index = (viewController as! PageContentViewControllerTest).pageIndex
        
        if index == NSNotFound {
            return nil
        }
        
        index++
        
        if(self.displayFavorites){
            if (index == Int(self.arrayOfRecords.count)) {
                return nil
            }
        }
        else{
            if (index == Int(ListeLoisParCategorie.count)) {
                return nil
            }
        }
        

        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject], transitionCompleted completed: Bool)
    {
        if(completed){
            self.currentLois = (pageViewController.viewControllers[0] as! PageContentViewControllerTest).detailLoisModel
            self.title = self.currentLois.title
        }
        
    }


    
    func viewControllerAtIndex(index: Int) -> PageContentViewControllerTest?
    {
        
        if(self.displayFavorites){
            if self.arrayOfRecords.count == 0 || index >= Int(arrayOfRecords.count)
            {
                return nil
            }
        }
        else{
            if self.ListeLoisParCategorie.count == 0 || index >= Int(ListeLoisParCategorie.count)
            {
                return nil
            }
        }
        
        
        
        // Create a new view controller and pass suitable data.
        let pageContentViewController = self.storyboard!.instantiateViewControllerWithIdentifier("PageContentViewControllerTest") as! PageContentViewControllerTest
        var indexPath = NSIndexPath(forRow: index, inSection: 0)
        if(self.displayFavorites){
            pageContentViewController.detailLoisModel = self.arrayOfRecords[UInt(indexPath.row)] as! Lois
        }else{
           pageContentViewController.detailLoisModel = self.ListeLoisParCategorie[UInt(indexPath.row)] as! Lois
        }
        
        pageContentViewController.pageIndex = index
        self.currentIndex = index

        
        return pageContentViewController
    }
   
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "showShareDialog"){

            let currentViewController: PageContentViewControllerTest = self.viewControllerAtIndex(currentIndex)!
            
            let shareDialog = segue.destinationViewController as! shareViewController
            shareDialog.loisModel = currentViewController.detailLoisModel
        }
    }
    
}

//
//  detailLoisCell.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 30/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit

@objc protocol actionLoisCellDelegate{
    optional func ajoutLoisFavoris()
    optional func voteLois(index:NSIndexPath)
    optional func voteLois()
}

class detailLoisBodyCell: UITableViewCell {
    
    var delegate:actionLoisCellDelegate?
    
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var isFavorite: UIButton!
    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var dateAjoutLabel: UILabel!
    @IBOutlet weak var categorieLabel: UILabel!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var nombre_commentaires: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func gestionFavoris(sender: UIButton) {
        self.delegate?.ajoutLoisFavoris!()
    }
    
    @IBAction func voteLois(sender: UIButton) {
        self.delegate?.voteLois!()
    }
    
}

//
//  containerConnectionViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 04/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import CoreText
import CoreGraphics
import TTTAttributedLabel

class containerConnectionViewController: UIViewController, TTTAttributedLabelDelegate{
   
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    @IBOutlet weak var termsAndConditions: TTTAttributedLabel!
    
    var clickedUrl:NSURL!
    var clickedtitle:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let linkColor = UIColor(red: 0.203, green: 0.329, blue: 0.835, alpha: 1)
        let linkActiveColor = UIColor.blackColor()
        
        self.termsAndConditions.delegate = self
//        self.termsAndConditions.linkAttributes = [NSForegroundColorAttributeName : linkColor]
//        self.termsAndConditions.activeLinkAttributes = [NSForegroundColorAttributeName : linkActiveColor]
        self.termsAndConditions.enabledTextCheckingTypes = NSTextCheckingType.Link.rawValue        

        self.termsAndConditions.addLinkToURL(NSURL(string: "http://www.google.com/?hl=fr&gws_rd=ssl#hl=fr&q=termes"), withRange: NSRange(location: 9,length: 6))
        self.termsAndConditions.addLinkToURL(NSURL(string: "http://www.google.com/?hl=fr&gws_rd=ssl#safe=off&hl=fr&q=conditions+g%C3%A9n%C3%A9rales"), withRange: NSRange(location: 19,length: 20))

    }
    
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        
        if(url.absoluteString == "http://www.google.com/?hl=fr&gws_rd=ssl#hl=fr&q=termes"){
            self.clickedtitle = "Termes"
        }
        else{
            self.clickedtitle = "Conditions générales"
        }
        
        //self.clickedUrl = url
        
//        var webVC = GDWebViewController()
//        webVC.delegate = self
//        
//        webVC.loadURLWithString(url!.absoluteString!)
//        webVC.toolbar.toolbarTintColor = UIColor.darkGrayColor()
//        webVC.toolbar.toolbarBackgroundColor = UIColor.whiteColor()
//        webVC.toolbar.toolbarTranslucent = false
//        webVC.allowsBackForwardNavigationGestures = true
//        webVC.showToolbar = true
//        webVC.title = self.clickedtitle
//        
//        self.presentViewController(webVC, animated: false, completion:nil)
        //performSegueWithIdentifier("showWebView", sender: self)
        

        var webBrowserNavigationController = KINWebBrowserViewController.navigationControllerWithWebBrowser()

        webBrowserNavigationController.navigationBar.translucent = false
        
        self.presentViewController(webBrowserNavigationController, animated: true){ () -> Void in
            UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        }
        
        var webVC = webBrowserNavigationController.rootWebBrowser()
        webVC.hidesBottomBarWhenPushed = true
        webVC.loadURL(url!)
        //webVC.tintColor = appDelegate.orange
        //webVC.barTintColor = appDelegate.orange
        webVC.showsPageTitleInNavigationBar = false
//        UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
//        [self presentViewController:webBrowserNavigationController animated:YES completion:nil];
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "showWebView" {
//            let controller = segue.destinationViewController as webViewController
////            controller.loadURLWithString(self.clickedUrl.absoluteString!)
////            controller.title = self.clickedtitle
//            controller.url = self.clickedUrl
////            controller.title = self.clickedtitle
//        }
    }
}

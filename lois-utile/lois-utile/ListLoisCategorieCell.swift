//
//  ListLoisCategorieCell.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 16/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit

class ListLoisCategorieCell: UITableViewCell {

    //@IBOutlet weak var body: UILabel!
    //@IBOutlet weak var titre: UILabel!
    var titre = UILabel(frame: CGRectMake(15, 17, 100, 12))
    var body = UILabel(frame: CGRectMake(15, 33, 290, 60))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    func setup(){
        titre.font = UIFont(name: "Helvetica-Light", size: 16.0)
      
        self.addSubview(titre)
        
        body.textColor = UIColor(red: 77.0/255, green: 69.0/255, blue: 62.0/255, alpha: 1.0)
        body.font = UIFont(name: "Helvetica-Light", size: 16.0)
        body.numberOfLines = 3
        self.addSubview(body)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ParamsViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 18/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import MessageUI

// Usage example:
//    let deviceType : DeviceTypes = UIDevice().deviceType
//    let deviceName : String = deviceType.rawValue

public enum DeviceTypes : String {
    case simulator      = "Simulator",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5c       = "iPhone 5c",
    iPad4          = "iPad 4",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    unrecognized   = "?unrecognized?"
}

public extension UIDevice {
    public var deviceType: DeviceTypes {
        var sysinfo : [CChar] = Array(count: sizeof(utsname), repeatedValue: 0)
        let modelCode = sysinfo.withUnsafeMutableBufferPointer {
            (inout ptr: UnsafeMutableBufferPointer<CChar>) -> DeviceTypes in
            uname(UnsafeMutablePointer<utsname>(ptr.baseAddress))
            // skip 1st 4 256 byte sysinfo result fields to get "machine" field
            let machinePtr = advance(ptr.baseAddress, Int(_SYS_NAMELEN * 4))
            var modelMap : [ String : DeviceTypes ] = [
                "i386"      : .simulator,
                "x86_64"    : .simulator,
                "iPad2,1"   : .iPad2,          //
                "iPad3,1"   : .iPad3,          // (3rd Generation)
                "iPhone3,1" : .iPhone4,        //
                "iPhone3,2" : .iPhone4,        //
                "iPhone4,1" : .iPhone4S,       //
                "iPhone5,1" : .iPhone5,        // (model A1428, AT&T/Canada)
                "iPhone5,2" : .iPhone5,        // (model A1429, everything else)
                "iPad3,4"   : .iPad4,          // (4th Generation)
                "iPad2,5"   : .iPadMini1,      // (Original)
                "iPhone5,3" : .iPhone5c,       // (model A1456, A1532 | GSM)
                "iPhone5,4" : .iPhone5c,       // (model A1507, A1516, A1526 (China), A1529 | Global)
                "iPhone6,1" : .iPhone5S,       // (model A1433, A1533 | GSM)
                "iPhone6,2" : .iPhone5S,       // (model A1457, A1518, A1528 (China), A1530 | Global)
                "iPad4,1"   : .iPadAir1,       // 5th Generation iPad (iPad Air) - Wifi
                "iPad4,2"   : .iPadAir2,       // 5th Generation iPad (iPad Air) - Cellular
                "iPad4,4"   : .iPadMini2,      // (2nd Generation iPad Mini - Wifi)
                "iPad4,5"   : .iPadMini2,      // (2nd Generation iPad Mini - Cellular)
                "iPhone7,1" : .iPhone6plus,    // All iPhone 6 Plus's
                "iPhone7,2" : .iPhone6         // All iPhone 6's
            ]
            if let model = modelMap[String.fromCString(machinePtr)!] {
                return model
            }
            return DeviceTypes.unrecognized
        }
        return modelCode
    }
}

class ParamsViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    
    /**
    Mode Déconnecté
    */
    let dataHeader:[Int:String] = [
        0:"Aide et support",
        1:"Gestion des notifications",
    ]
    
    let dataSupport:[Int:String] = [
        0:"Signaler un bug",
        1:"Proposer une amélioration",
        2:"Contacter le support"
    ]
    
    let dataNotifications:[Int:String] = [
        0:"Personnaliser les notifications",
    ]
    /**
    Mode Connecté
    */
    let datahHeaderAccount:[Int:String] = [
        0:"Votre compte",
    ]
    
    let dataAccount:[Int:String] = [
        0:"Deconnexion"
    ]
    
    /*
    Liste de toutes les catégories de l'application
    */
    var categories = Categorie.allObjects()
    
    
    var showAllData:Bool = true
    
    override func viewDidLoad() {

    }

    override func viewWillAppear(animated: Bool) {
        // Je crée un listener qui lors de la deconection renvera sur la home.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "PreviousPage",
            name: "finishDestroyUser", object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "finishDestroyUser", object: nil)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(self.showAllData){
            return dataHeader.count + datahHeaderAccount.count
        }
        else{
            return dataHeader.count
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(self.showAllData){
            if(section == 0){
                return dataAccount.count
            }
            else if(section == 1){
                return dataSupport.count
            }
            else{
                return dataNotifications.count
            }
        }
        else{
            if(section == 1){
                return dataSupport.count
            }
            else{
                return dataNotifications.count
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(self.showAllData){
            if(indexPath.section == 0){
                let cell = UITableViewCell()
                cell.textLabel?.text = dataAccount[indexPath.row]
                return cell
            }
            else if(indexPath.section == 1){
                let cell = UITableViewCell()
                cell.textLabel?.text = dataSupport[indexPath.row]
                return cell
            }
            else{
                let cell = UITableViewCell()
                cell.textLabel?.text = dataNotifications[indexPath.row]
                return cell
            }
        }
        else{
            if(indexPath.section == 1){
                let cell = UITableViewCell()
                cell.textLabel?.text = dataSupport[indexPath.row]
                return cell
            }
            else{
                let cell = UITableViewCell()
                cell.textLabel?.text = dataNotifications[indexPath.row]
                return cell
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("headerCellParametres") as! headerCellParametres
        
        if(self.showAllData){
            if(section == 0){
                headerCell.headerLabel.text = dataAccount[section]
            }
            else if(section == 1){
                headerCell.headerLabel.text = dataHeader[0]
            }
            else{
                headerCell.headerLabel.text = dataHeader[1]
            }
        }
        else{
            if(section == 1){
                headerCell.headerLabel.text = dataHeader[0]
            }
            else{
                headerCell.headerLabel.text = dataHeader[1]
            }
        }
        
        return headerCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(self.showAllData){
            if(indexPath.section == 0){
                println("J'ai fais un choix dans Account")
                if(indexPath.row == 0){
                    println("Deconnection")
                    var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
                    TAOverlay.showOverlayWithLabel("Deconnection",options: options)
                    
                    datasSingleton.sharedInstance.logout_user()
                }
            }
            else if(indexPath.section == 1){
                if(indexPath.row == 0){
                    sendMailBug()
                }
                else if(indexPath.row == 1){
                    sendMailAmelioration()
                }
                else if(indexPath.row == 2){
                    sendMailSupport()
                }
            }
            else{
                println("Selection Notifications")
                self.performSegueWithIdentifier("showNotificationConfiguration", sender: self)
            }
        }
        else{
            if(indexPath.section == 1){
                if(indexPath.row == 0){
                    sendMailBug()
                }
                else if(indexPath.row == 1){
                    sendMailAmelioration()
                }
                else if(indexPath.row == 2){
                    sendMailSupport()
                }
            }
            else{
                println("Selection Notifications")
                self.performSegueWithIdentifier("showNotificationConfiguration", sender: self)
            }
        }
    }
    
    /*
    A la deconnection je renvois sur la page d'accueil.
    */
    func PreviousPage(){
        
        dispatch_async(dispatch_get_main_queue(), {
            TAOverlay.hideOverlay()
        })
        
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("backToPreviousPage", object: nil)
        })
    }
    
    @IBAction func hide(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func sendMailBug(){
        
        //First get the nsObject by defining as an optional anyObject
        let nsObject: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
        
        //Then just cast the object as a String, but be careful, you may want to double check for nil
        let version = nsObject as! String
        
        var emailTitle = "Signalement de bug"
        var messageBody = "\n\n\n---\n\nVersion de l'appareil iOS : \(UIDevice.currentDevice().deviceType.rawValue) \n\n Version iOS : \(UIDevice.currentDevice().systemVersion) \n\n Version de l'application : \(version)"
        var toRecipents = ["support@lois-utiles.fr"]
        displayMail(emailTitle,messageBody: messageBody,toRecipents: toRecipents)
    }
    
    func sendMailAmelioration(){
        var emailTitle = "Proposition d'amélioration"
        var messageBody = ""
        var toRecipents = ["support@lois-utiles.fr"]
        displayMail(emailTitle,messageBody: messageBody,toRecipents: toRecipents)
    }
    
    func sendMailSupport(){
        var emailTitle = "Contact support"
        var messageBody = ""
        var toRecipents = ["support@lois-utiles.fr"]
        displayMail(emailTitle,messageBody: messageBody,toRecipents: toRecipents)
    }
    
    func displayMail(emailTitle:String,messageBody:String,toRecipents:[AnyObject]){
        var mc:MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        //mc.navigationBar.translucent = false
        mc.navigationBar.tintColor = UIColor.whiteColor()
        self.presentViewController(mc, animated: true) { () -> Void in
            UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        }
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError) {
        switch result.value {
        case MFMailComposeResultCancelled.value:
            println("Mail cancelled")
        case MFMailComposeResultSaved.value:
            println("Mail saved")
        case MFMailComposeResultSent.value:
            println("Mail sent")
        case MFMailComposeResultFailed.value:
            println("Mail sent failure: %@", [error.localizedDescription])
        default:
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    

}

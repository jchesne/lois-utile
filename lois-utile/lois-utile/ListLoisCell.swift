//
//  CollectionViewCellDernieres.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 16/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class ListLoisCell: UITableViewCell {

    var delegate:actionLoisCellDelegate?
    var index:NSIndexPath!
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)

    
    @IBOutlet weak var voteButton: UIButton!
    
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        initialize()
//    }
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        initialize()
//    }
//    override init() {
//        super.init()
//        initialize()
//    }
//    
//    func initialize(){
//        self.voteButton.layer.borderWidth = 1
//        self.voteButton.layer.borderColor = appDelegate.gris.CGColor
//    }
    
    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var date_ajout: TTTAttributedLabel!
    
    @IBOutlet weak var vote_plus: UIButton!
   // @IBOutlet weak var vote_moins: UIButton!
    @IBOutlet weak var nombre_commentaires: UILabel!

//    override init(frame: CGRect) {
//        
//        
//        
//        super.init(frame: frame)
//    }
    
    @IBAction func gestionFavoris(sender: UIButton) {
        self.delegate?.ajoutLoisFavoris!()
    }
    
    @IBAction func voteLois(sender: UIButton) {
        self.delegate?.voteLois!(self.index)
    }
}

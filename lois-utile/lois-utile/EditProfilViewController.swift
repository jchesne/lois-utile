//
//  EditProfilViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 09/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import MobileCoreServices

class EditProfilViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL
    let directoryURLString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    let dataSingleton = datasSingleton.sharedInstance
    
    @IBOutlet weak var prenomTextField: FloatLabelTextField!
    @IBOutlet weak var nomTextField: FloatLabelTextField!
    @IBOutlet weak var avatarUser: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
        var imageData :NSData = NSData(contentsOfURL:nsurl)!
        
        self.prenomTextField.text = UserModel.getUser()!.prenom
        self.nomTextField.text = UserModel.getUser()!.nom
        self.avatarUser.image = UIImage(data: imageData)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func annulerEdition(sender: UIBarButtonItem) {
        //Avant de dissmiss je regarde si on a un pointeur sur un champs.
        if(self.nomTextField.isFirstResponder() || self.prenomTextField.isFirstResponder()){
            self.view.endEditing(true)
        }
        else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBAction func modifierAvatar(sender: UIButton) {
        var refreshAlert = UIAlertController(title: nil, message: "Comment voulez-vous modifier votre photo de profil ?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        refreshAlert.addAction(UIAlertAction(title: "Prendre une photo", style: .Default, handler: { (action: UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                var cameraController = UIImagePickerController()
                cameraController.delegate = self
                cameraController.sourceType = UIImagePickerControllerSourceType.Camera
                
                let mediaTypes:[AnyObject] = [kUTTypeImage]
                cameraController.mediaTypes = mediaTypes
                cameraController.allowsEditing = false
                
                self.presentViewController(cameraController, animated: true, completion: nil)
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Choisir une photo", style: .Default, handler: { (action: UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                
                var photoLibraryController = UIImagePickerController()
                photoLibraryController.delegate = self
                photoLibraryController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                
                let mediaTypes:[AnyObject] = [kUTTypeImage]
                photoLibraryController.mediaTypes = mediaTypes
                photoLibraryController.allowsEditing = true
                
                self.presentViewController(photoLibraryController, animated: true, completion: nil)
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) in

        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
        
        UIView.transitionWithView(self.avatarUser,
            duration:1,
            options: UIViewAnimationOptions.TransitionCrossDissolve,
            animations: { self.avatarUser.image = image },
            completion: nil)
        
        saveNewImage()
    }
    
    func saveNewImage(){
        
        var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Enregistrement de l'image",options: options)
        
        var request = NSMutableURLRequest(URL: NSURL(string: "\(appDelegate.url)/api/v1/fr/user/update_avatar")!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        
        var image = RBResizeImage(self.avatarUser.image,CGSizeMake(128,128))
        var imageData = UIImagePNGRepresentation(image)
        var base64String = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)) // encode the image
        
        var err: NSError?
        var params = ["image":["filename":"user_\(UserModel.getUser()!.uid)_avatar.jpg", "file_data": base64String]]
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions(0), error: &err)!
        
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            var err: NSError?
            
            dispatch_async(dispatch_get_main_queue(), {
                TAOverlay.hideOverlay()
            })
            
            //Si j'ai bien modifié l'image, ça sert à rien de refaire un appel à drupal pour récupérer l'image, je vais utiliser cette que je viens de selectionner.
            let fileManager = NSFileManager.defaultManager()
            var nsurl = self.directoryURLString.stringByAppendingPathComponent("avatar/user_avatar.jpg")
            var imageData: NSData = UIImagePNGRepresentation(self.avatarUser.image)
            fileManager.createFileAtPath(nsurl, contents: imageData, attributes: nil)
        })
        
        task.resume() // this is needed to start the task
    }
    

    @IBAction func updateUser(sender: UIBarButtonItem) {
        dataSingleton.updateUser(self.prenomTextField.text, lastname: self.nomTextField.text,callback: {
            dispatch_async(dispatch_get_main_queue(), {
                self.dismissViewControllerAnimated(true, completion: nil)
            })
        })
    }
    
    
    @IBAction func supprimerCompte(sender: UIButton) {
        var refreshAlert = UIAlertController(title: nil, message: "Confirmation", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        refreshAlert.addAction(UIAlertAction(title: "Supprimer mon compte", style: .Default, handler: { (action: UIAlertAction!) in
            println("On supprime le compte")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
}

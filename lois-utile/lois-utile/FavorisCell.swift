//
//  FavorisCell.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 14/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit

class FavorisCell: UITableViewCell {

    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var ajoutDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ListeRessourcesTableViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 09/12/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit

class ListeRessourcesTableViewController: UITableViewController {

    var ressourcesArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if(ressourcesArray.count > 0){
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
            return ressourcesArray.count
        }
        else{
            var message:UILabel = UILabel()
            message.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
            
            message.text = "Aucune ressource rensseignées.";
            message.textColor = UIColor.blackColor()
            message.numberOfLines = 0;
            message.textAlignment = NSTextAlignment.Center;
            message.font = UIFont(name: "Helvetica-Light", size: 14.0)
            message.sizeToFit()
            
            self.tableView.backgroundView = message;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
            
            return 0
        }
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RessourcesCell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = self.ressourcesArray[indexPath.row]
        return cell
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
////        if segue.identifier == "showWebView" {
////            let controller = segue.destinationViewController as webViewController
////            var index = self.tableView.indexPathForSelectedRow()!
////            controller.url = NSURL(string: self.ressourcesArray[index.row])
////            controller.title = "Ressources"
////        }
//    }
    
    

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        showWebView()
    }
    
    func showWebView(){
        

        var index = self.tableView.indexPathForSelectedRow()!
        var url = NSURL(string: self.ressourcesArray[index.row])
        var webVC = KINWebBrowserViewController()
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
        webVC.loadURL(url!)
        
//        webVC.delegate = self
//        
//        var index = self.tableView.indexPathForSelectedRow()!
//        var url = NSURL(string: self.ressourcesArray[index.row])
//
//        
//        webVC.loadURLWithString(url!.absoluteString!)
//        webVC.toolbar.toolbarTintColor = UIColor.darkGrayColor()
//        webVC.toolbar.toolbarBackgroundColor = UIColor.whiteColor()
//        webVC.toolbar.toolbarTranslucent = false
//        webVC.allowsBackForwardNavigationGestures = true
//        webVC.showToolbar = true
//        
//
        
    }
}

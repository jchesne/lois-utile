//
//  ViewController.swift
//  UICollectionView
//
//  Created by Brian Coleman on 2014-09-04.
//  Copyright (c) 2014 Brian Coleman. All rights reserved.
//

import UIKit
import Realm

//protocol sortListDelegate{
//    func sortList(index:Int)
//}

class Accueil: UIViewController, UITableViewDataSource, UITableViewDelegate, FBLoginViewDelegate, finishLoadingDelegate, actionLoisCellDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var user_avatar_icon: UIButton!
    
    var currentIem:Int = 0
    var liste_Lois = Lois.allObjects().sortedResultsUsingProperty("changed", ascending: false)
    let api = API()
    let dataSingleton = datasSingleton.sharedInstance
    var currentSelectedFilter = 0
    var scrollPosition:CGPoint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    let directoryURLString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
    let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL
    

//    var fetchedResultsControllerLois:NSFetchedResultsController = NSFetchedResultsController()
//    var fetchedCategories:NSFetchedResultsController = NSFetchedResultsController()
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
   // let managedObjectContext = (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    
    var connecteAInternet:Bool = false
    
    var deja_averti_non_connecte = false
    
    var notificationToken: RLMNotificationToken?
    
    
    func loadingFinish() {
        // J'ai terminé de charger les paramètre de l'utilisateur, maintenant je vais télécharger son avatar.
        update_avatar()
        // Je remet la couleur blanche en fond
        appDelegate.window?.backgroundColor = UIColor.whiteColor()
        self.tabBarController?.view.alpha = 1
    }

    

    override func viewDidLoad() {
        
        self.tabBarController?.view.alpha = 0
        
        // Affiche la loadingView en dessus de la page pour afficher les citations et faire le chargement.
        let vc : SplashScreen = storyboard!.instantiateViewControllerWithIdentifier("SplashScreen") as! SplashScreen;
        
        vc.delegate = self
        
        self.tabBarController?.presentViewController(vc, animated: false,completion: nil)
        
        
        //self.tableView.
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 166.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "noInternet",
            name: "noInternet", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "noInternetAlert",
            name: "noInternetAlert", object: nil)
        
        tableView.registerNib(UINib(nibName: "ListLoisCell", bundle: nil), forCellReuseIdentifier: "ListLoisCell")
        
        super.viewDidLoad()
        
        
        
        notificationToken = RLMRealm.defaultRealm().addNotificationBlock { note, realm in
            self.tableView.reloadData()
            println("JE RELOAD LE TABLEAU")
        }
        
        
        if Reachability.isConnectedToNetwork() {
            // Je récupère tout le contenu du site
            api.getContents()
        }
        
        // J'indique à mon tableView qu'il peut gérer les données, ça va appeller cellForItemAtIndexPath
        self.tableView.dataSource = self
        self.tableView.delegate = self
        //self.collectionViewFlowLayout.itemSize = CGSize(width: (self.view.frame.width/2), height: 200)
        
         self.segmentControl.addTarget(self, action:"segmentSwitch:" , forControlEvents:UIControlEvents.ValueChanged)
        
        
        self.tableView.addPullToRefresh({ [weak self] in
            println("Faire appel au code de mise à jour des lois.")
            // refresh code
//            
//            self?.tableView.reloadData()
//            self?.tableView.stopPullToRefresh()
        })
        
        
        
//        [super viewDidLoad];
//        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        [self.navigationItem setBackBarButtonItem:backButtonItem];
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.tableView.fixedPullToRefreshViewForDidScroll()
    }
    
    override func viewDidAppear(animated: Bool) {
       update_avatar()
    }
    
    func update_avatar(){
        if(UserModel.getUser() != nil){
            let filemanager = NSFileManager.defaultManager()
            var fichier = self.directoryURLString.stringByAppendingPathComponent("avatar/user_avatar.jpg")
            if(filemanager.fileExistsAtPath(fichier)){
                var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
                var imageData :NSData = NSData(contentsOfURL:nsurl)!
                
                let avatar_image = UIImage(data: imageData)
                
                if(self.user_avatar_icon.imageForState(UIControlState.Normal) == nil){
                    UIView.transitionWithView(self.user_avatar_icon,
                        duration:1,
                        options: UIViewAnimationOptions.TransitionCrossDissolve,
                        animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                        completion: nil)
                }
                else{
                    let data1:NSData = UIImagePNGRepresentation(self.user_avatar_icon.imageForState(UIControlState.Normal))
                    let data2:NSData = UIImagePNGRepresentation(avatar_image)
                    
                    if(!data1.isEqual(data2)){
                        UIView.transitionWithView(self.user_avatar_icon,
                            duration:1,
                            options: UIViewAnimationOptions.TransitionCrossDissolve,
                            animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                            completion: nil)
                    }
                }
            }
        }
        else{
            self.user_avatar_icon.setImage(UIImage(named: "user"), forState: UIControlState.Normal)
        }
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCellList") as HeaderCellList
//           // headerCell.headerLabel.text = "";
//        headerCell.iniSegmentControl(self.currentSelectedFilter)
//        headerCell.delegate = self
//        return headerCell
//    }
    
    func segmentSwitch(control:UISegmentedControl) {
        switch (control.selectedSegmentIndex) {
        //récents
        case 0:
            liste_Lois = Lois.allObjects().sortedResultsUsingProperty("changed", ascending: false)
            break;
        //top
        case 1:
            liste_Lois = Lois.allObjects().sortedResultsUsingProperty("vote_plus", ascending: false)
            break;
        //commentées
        case 2:
            liste_Lois = Lois.allObjects().sortedResultsUsingProperty("comments_count", ascending: false)
            break;
        default:
            break;
        }
        
        // Je récupère l'index pour l'attribuer à nouveau à mon segment
       // self.currentSelectedFilter = index
        self.tableView.reloadData()
        self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
    }
    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//         return CGFloat(44)
//    }
    
    // Quoiqu'il en soit on alerte l'utilisateur qu'il n'a pas internet, action volontaire de l'utilisateur d'utiliser une fonction qui necessite internet.
    func noInternetAlert(){
            var options = (TAOverlayOptions.AllowUserInteraction | TAOverlayOptions.AutoHide | TAOverlayOptions.OverlayTypeWarning | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel("Vérifiez votre connection internet. Utilisation limitée hors connection.",options: options)
    }
    
    //Ici on va éviter de le saouler, alors on va afficher l'alerte une seule fois. Par example action non volontaire de fetch des commentaires.
    func noInternet(){
        if(!self.deja_averti_non_connecte){
            var options = (TAOverlayOptions.AllowUserInteraction | TAOverlayOptions.AutoHide | TAOverlayOptions.OverlayTypeWarning | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel("Vérifiez votre connection internet. Utilisation limitée hors connection.",options: options)
            self.deja_averti_non_connecte = true
        }
    }
    
    
//    func controllerDidChangeContent(controller: NSFetchedResultsController) {
//        self.tableView.reloadData()
//    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->
        Int {
        if(Int(liste_Lois.count) > 0){
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
            return Int(liste_Lois.count)
        }
        else{
            var message:UILabel = UILabel()
            message.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
            
            message.text = "Il n'y a aucune lois. oups...";
            message.textColor = UIColor.blackColor()
            message.numberOfLines = 0;
            message.textAlignment = NSTextAlignment.Center;
            message.font = UIFont(name: "OpenSans", size: 14.0)
            message.sizeToFit()
            
            self.tableView.backgroundView = message;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
            
            return 0
        }
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListLoisCell", forIndexPath: indexPath) as! ListLoisCell
        //cell.backgroundColor = UIColor.blackColor()
        

        //var lois = fetchedResultsControllerLois.objectAtIndexPath(indexPath) as LoisModel
        var lois = liste_Lois[UInt(indexPath.row)] as! Lois
        cell.titre.text = lois.title
     //   cell.categorie.text = lois.categorie_name
        cell.body.text = lois.body
        cell.vote_plus.setTitle("\(lois.vote_plus)", forState: UIControlState.Normal)
        cell.nombre_commentaires.text = "\(lois.comments_count)"
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        var date_converted = NSDate(timeIntervalSince1970: (lois.changed as NSString).doubleValue)
        let date = dateFormatter.stringFromDate(date_converted)
        
        let text = "ajouté le \(date) dans \(lois.categorie_name)"
        let attributedString = NSMutableAttributedString(string: text)
        cell.date_ajout.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (NSMutableAttributedString) -> NSMutableAttributedString! in
            var stringRange = (text as NSString).rangeOfString(lois.categorie_name)
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "OpenSans", size: 10.0)!, range: NSRange(location: 0,length: (text as NSString).length))
            
            attributedString.addAttribute(kCTForegroundColorAttributeName as! String, value: self.appDelegate.gris_button_tabbar_inactif, range: NSRange(location: 0,length: (text as NSString).length))
            
            attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "OpenSans", size: 10.0)!, range: stringRange)
            
            attributedString.addAttribute(kCTForegroundColorAttributeName as! String, value: self.appDelegate.gris_button_tabbar_actif, range: stringRange)
            
           return attributedString
        })
      //  cell.vote_moins.setTitle("\(lois.vote_moins)", forState: UIControlState.Normal)
        cell.delegate = self
        cell.layoutIfNeeded()
        cell.index = indexPath
        
        cell.voteButton.layer.borderWidth = 1
        cell.voteButton.layer.borderColor = appDelegate.gris.CGColor
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        
        return cell
    }

    
    func voteLois(index:NSIndexPath){
//        if(!self.dataSingleton.scroll_deja_handled){
//            self.scrollPosition = CGPointMake(0.0, self.tableView.contentOffset.y - 31.5)
//            self.dataSingleton.scroll_deja_handled = true
//        }
//        else{
//            self.scrollPosition = CGPointMake(0.0, self.tableView.contentOffset.y)
//        }
        
       // self.tableView.contentOffset
        
        self.scrollPosition = CGPointMake(0.0, self.tableView.contentOffset.y)
        
        println(self.scrollPosition)
        self.tableView.reloadData()
        var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
        println("JE VOTE")
    }
    
    func update() {
        //   if(self.scrollPosition != nil){
        self.tableView.contentOffset = self.scrollPosition
        self.tableView.layoutIfNeeded()
        // }
    }
    
//    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
//        println(scrollView.contentOffset)
//
//        //self.maybeShowGotoButtons()
//    }
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]?  {
        
        var moreRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "More", handler:{action, indexpath in
            println("MORE•ACTION");
        });
        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);
        
        var deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
            println("DELETE•ACTION");
        });
        
        return [deleteRowAction, moreRowAction];
//        var shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "Share" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
//            // 2
//            let shareMenu = UIAlertController(title: nil, message: "Share using", preferredStyle: .ActionSheet)
//            
//            let twitterAction = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.Default, handler: nil)
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
//            
//            shareMenu.addAction(twitterAction)
//            shareMenu.addAction(cancelAction)
//            
//            
//            self.presentViewController(shareMenu, animated: true, completion: nil)
//        })
//        // 3
//        var rateAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Rate" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
//            // 4
//            let rateMenu = UIAlertController(title: nil, message: "Rate this App", preferredStyle: .ActionSheet)
//            
//            let appRateAction = UIAlertAction(title: "Rate", style: UIAlertActionStyle.Default, handler: nil)
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
//            
//            rateMenu.addAction(appRateAction)
//            rateMenu.addAction(cancelAction)
//            
//            
//            self.presentViewController(rateMenu, animated: true, completion: nil)
//        })
//        // 5
        //return [shareAction,rateAction]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ListeLastToDetail", sender: tableView)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ListeLastToDetail" {
            let indexPath: AnyObject? = self.tableView.indexPathForSelectedRow()
            let detailLoisViewController = segue.destinationViewController as! UILoisPageViewController
            detailLoisViewController.currentIndex = 0
            
            var selectedLois = liste_Lois[UInt(indexPath!.row)] as! Lois
            


            detailLoisViewController.ListeLoisParCategorie = Lois.objectsWhere("nid = '\(selectedLois.nid)'")
//            let detailLoisViewController = segue.destinationViewController as PageContentViewController
//            let indexPath: AnyObject = tableView.indexPathForSelectedRow()!
//            let selectedLois:Lois = liste_Lois[UInt(indexPath.row)] as Lois
//            detailLoisViewController.detailLoisModel = selectedLois
        }
    }

    
    @IBAction func connectionButton(sender: UIButton) {
        if((UserModel.getUser()) != nil){
            self.performSegueWithIdentifier("showCompteDetail", sender: self)
        }
        else{
             self.performSegueWithIdentifier("showCreationCompte", sender: self)
        }
    }
    
    @IBAction func unwindToHomeScreen(segue:UIStoryboardSegue) {
        println("ICI")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//
//  avantCreationCompte.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 19/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//
import UIKit;

class avantCreationCompte: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        println("init listener")
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "backToPreviousPageHandler",
            name: "backToPreviousPage", object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "backToPreviousPage", object: nil)
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showParamsFromLogin" {
            
            let navigationControllerDestination = segue.destinationViewController as! UINavigationController
            
            let paramViewController = navigationControllerDestination.topViewController as! ParamsViewController
            paramViewController.showAllData = false
        }
    }

    /*
    Fonction qui va renvoyer à la page précédente quand l'utilisateur clique sur la croix sur la page 
    de creation de compte, il annule l'action et donc on le renvois sur la page initiale.
    */
    func backToPreviousPageHandler(){
        self.navigationController?.popViewControllerAnimated(true)
    }
}

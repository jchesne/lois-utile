//
//  CategoriesListeViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 23/09/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import Foundation
import UIKit
import Realm

class ListeCategoriesViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{

    var liste_Categorie = Categorie.allObjects()
     @IBOutlet weak var user_avatar_icon: UIButton!
    
    var currentIem:Int = 0
    var pair:Bool = true

    var number = 0

    let directoryURLString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
    let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL

    
    var selected_tid:String!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewFlowLayout.itemSize = CGSize(width: (self.view.frame.width/2), height: 110)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        update_avatar()
    }
    

    func update_avatar(){
        if(UserModel.getUser() != nil){
            let filemanager = NSFileManager.defaultManager()
            var fichier = self.directoryURLString.stringByAppendingPathComponent("avatar/user_avatar.jpg")
            if(filemanager.fileExistsAtPath(fichier)){
                var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
                var imageData :NSData = NSData(contentsOfURL:nsurl)!
                
                let avatar_image = UIImage(data: imageData)
                
                if(self.user_avatar_icon.imageForState(UIControlState.Normal) == nil){
                    UIView.transitionWithView(self.user_avatar_icon,
                        duration:1,
                        options: UIViewAnimationOptions.TransitionCrossDissolve,
                        animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                        completion: nil)
                }
                else{
                    let data1:NSData = UIImagePNGRepresentation(self.user_avatar_icon.imageForState(UIControlState.Normal))
                    let data2:NSData = UIImagePNGRepresentation(avatar_image)
                    
                    if(!data1.isEqual(data2)){
                        UIView.transitionWithView(self.user_avatar_icon,
                            duration:1,
                            options: UIViewAnimationOptions.TransitionCrossDissolve,
                            animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                            completion: nil)
                    }
                }
            }
        }
        else{
            self.user_avatar_icon.setImage(UIImage(named: "user"), forState: UIControlState.Normal)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(liste_Categorie.count)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellCategories", forIndexPath: indexPath) as! CollectionViewCellCategories
        //cell.backgroundColor = UIColor.blackColor()
        
        var categorie:Categorie = liste_Categorie[UInt(indexPath.row)] as! Categorie
        cell.nom.text = categorie.name.uppercaseString
        cell.count.text =  "\(categorie.lois.count) lois utiles"
        
        if(pair){
            if(currentIem < 1){
                ++currentIem
            }
            else{
                pair = false
                currentIem = 0
                
            }
            cell.backgroundColor = UIColor(red: 234.0/255.0, green: 232.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        }
        else{
            if(currentIem < 1){
                ++currentIem
            }
            else{
                
                pair = true
                currentIem = 0
                
            }
            cell.backgroundColor = UIColor(red: 244.0/255.0, green: 243.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        }
        
        
        return cell
    }
    
//    func categorieFetchRequest() -> NSFetchRequest {
//        let fetchRequest = NSFetchRequest(entityName: "LoisModel")
//        fetchRequest.predicate = NSPredicate(format: "tid = %@", self.selected_tid)
//        let sortDescriptor = NSSortDescriptor(key: "nid", ascending: false)
//        fetchRequest.sortDescriptors = [sortDescriptor]
//        return fetchRequest
//    }
//    
//    func getFetchedResultsController(modelToFetch:()-> NSFetchRequest) -> NSFetchedResultsController {
//        var fetchedResultsController = NSFetchedResultsController(fetchRequest: modelToFetch(), managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
//        return fetchedResultsController
//    }
    
    // MARK: - Navigation
    

    @IBAction func connectionButton(sender: UIButton) {
        if((UserModel.getUser()) != nil){
            self.performSegueWithIdentifier("showCompteDetail", sender: self)
        }
        else{
            self.performSegueWithIdentifier("showCreationCompte", sender: self)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "categorieToLoisCategorie" {
            let selectedIndex = self.collectionView.indexPathForCell(sender as! UICollectionViewCell)!
            //self.selected_tid = (tableau_categories.objectAtIndexPath(selectedIndex) as CategorieModel).tid
            
            
//            ListeLoisParCategorie = getFetchedResultsController(categorieFetchRequest)
//            ListeLoisParCategorie.delegate = self
//            ListeLoisParCategorie.performFetch(nil)
            
            var currentCategorie = liste_Categorie[UInt(selectedIndex.row)] as! Categorie
            
            let controller = segue.destinationViewController as! ListeLoisCategoriesControllerTableViewController
            //controller.ListeLoisParCategorie = filteredLois
            controller.title = currentCategorie.name
            controller.ListeLoisParCategorie = currentCategorie.lois
        }
    }
    
}
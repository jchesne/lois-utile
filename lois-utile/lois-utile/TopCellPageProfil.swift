//
//  TopCellPageProfil.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 05/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class TopCellPageProfil: UITableViewCell {

    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nomLabel: UILabel!
    
    @IBOutlet weak var editerButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.editerButton.layer.borderWidth = 1
        self.editerButton.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5).CGColor
        
        self.avatarImage.layer.borderWidth = 1
        self.avatarImage.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

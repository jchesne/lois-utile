//
//  UserCommentCell.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 04/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class UserCommentCell: UITableViewCell {

    @IBOutlet weak var dateLabel: TTTAttributedLabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func supprimerComment(sender: UIButton) {
    }
}


import Foundation
import UIKit
import Realm

let FB = Facebook();

class Facebook{
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var check_can_post = false
    
    var fbSession:FBSession?;
    init(){
        self.fbSession = FBSession.activeSession();
    }
    
    func hasActiveSession() -> Bool{
        let fbsessionState = FBSession.activeSession().state;
        if ( fbsessionState == FBSessionState.Open
            || fbsessionState == FBSessionState.OpenTokenExtended ){
                self.fbSession = FBSession.activeSession();
                return true;
        }
        return false;
    }
    
    func login(callback: () -> Void?){
        
        let permission = ["public_profile", "email", "user_friends"];
        
        let activeSession = FBSession.activeSession();
        let fbsessionState = activeSession.state;
        var showLoginUI = true;
        
        if(fbsessionState == FBSessionState.CreatedTokenLoaded){
            showLoginUI = false;
        }
        
        if(fbsessionState != FBSessionState.Open
            && fbsessionState != FBSessionState.OpenTokenExtended){
                FBSession.openActiveSessionWithReadPermissions(
                    permission,
                    allowLoginUI: showLoginUI,
                    completionHandler: { (session:FBSession!, state:FBSessionState, error:NSError!) in
                        
                        if(error != nil){
                            println("Session Error: \(error)");
                        }
                        
                        self.fbSession = session;
                        
                        // If the session was opened successfully
                        if  state == FBSessionState.Open
                        {
                            println("SESSION OPENED")
                        }
                        // If the session closed
                        if state == FBSessionState.Closed
                        {
                            println("SESSION CLOSED")
                        }
                        
                        if(!self.check_can_post){
                            callback()
                        }
                    }
                )
                return
        }
        
        callback()
    }
    
    func shareContent(lois:Lois!){
        let permission = "publish_actions"
        let activeSession = self.fbSession!;
        
        check_can_post = true
        
        // Si l'utilisateur n'est pas connecté à facebook, je lui demande de se connecter
        if(!hasActiveSession()){
            println("Il n'y a pas de session active.")
            let fbsessionState = activeSession.state;
            var showLoginUI = true;
            
            if(fbsessionState == FBSessionState.CreatedTokenLoaded){
                showLoginUI = false;
            }
            
            if(fbsessionState != FBSessionState.Open
                && fbsessionState != FBSessionState.OpenTokenExtended){
                    FBSession.openActiveSessionWithReadPermissions(
                        [permission],
                        allowLoginUI: showLoginUI,
                        completionHandler: { (session:FBSession!, state:FBSessionState, error:NSError!) in
                            
                            if(error != nil){
                                println("Session Error: \(error)");
                            }
                            
                            self.fbSession = session;
                            
                            self.check_can_post = false
                            
                            self.posttoFB(lois)
                            
                        }
                    );
                    return;
            }
        }
        else{
            println("J'ai une session active, je regarde si j'ai le droit de poster.")
            // j'ai une session active, je regarde si j'ai le droit de publier
            if((find(activeSession.permissions as AnyObject as! [String],permission)) == nil){
                self.fbSession?.requestNewPublishPermissions([permission], defaultAudience: FBSessionDefaultAudience.Everyone, completionHandler: { (session:FBSession!, error:NSError!) -> Void in
                    if(error != nil){
                        println("Session Error: \(error)");
                        println("Permissions denied")
                    }
                    else{
                        println("J'ai le droit, je poste.")
                        self.check_can_post = false
                        
                        self.posttoFB(lois);
                    }
                })
            }
            // j'ai déjà une session avec le droit de poster.
            else{
                posttoFB(lois)
            }
        }
    }
    
    func posttoFB(lois:Lois!){
        println("Je vais poster sur FB")
        var text = (lois.body as NSString)
        if(text.length > 200){
            text = text.substringToIndex(250)+"..."
        }
        FBWebDialogs.presentFeedDialogModallyWithSession(self.fbSession, parameters: ["name": lois.title, "caption": "Toutes les lois utiles sont sur lois-utiles.fr !", "description": text, "link": lois.link]) { (result, resultURL, error:NSError!) -> Void in
            if (error != nil){
                println("error publishing \(error.description)")
            } else {
                if (result == FBWebDialogResult.DialogNotCompleted){
                    println("user cancelled")
                } else {
                    dispatch_async(dispatch_get_main_queue(), {
                        var options = (TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect | TAOverlayOptions.AutoHide)
                        TAOverlay.showOverlayWithLabel(nil,options: options)
                    })
                }
            }
        }
    }
    
    func logout(){
        self.fbSession?.closeAndClearTokenInformation();
        self.fbSession?.close();
    }
    
    func getInfo(loginAfterGettingInfo:Bool = false, useToken:Bool = false, displayLoader:Bool = false){
        FBRequest.requestForMe()?.startWithCompletionHandler({(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) in
            if(error != nil){
                println("Error Getting ME: \(error)");
            }
            
            if(loginAfterGettingInfo){
                self.initFacebookUser(result,useToken: useToken, displayLoader: displayLoader)
            }
            
        });
    }
    
    /*
        Cette fonction permet d'initialiser la creation + login ou juste login d'un utilisateur facebook.
    */
    func initFacebookUser(result:AnyObject, useToken:Bool=false, displayLoader:Bool = false){
        if(displayLoader){
            var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel("Connexion en cours...",options: options)
        }
        
        
        var params = [
        "type":"fb",
        "account":[[
            "fb_id":result.objectID,
            "fb_firstname":result.first_name,
            "fb_lastname":result.last_name,
            "fb_username":result.name,
            "fb_email":result.objectForKey("email") as! String,
            "fb_token":self.fbSession!.accessTokenData.accessToken
            ]]
        ]
        
        var err: NSError?
        var data = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        
        self.post(useToken: useToken,type:"registerFacebookUser",params: data!, url: "\(appDelegate.url)/api/v1/fr/user/createlogin") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                //Je modifie l'utilisateur et enregistre le facebooktoken.
                datasSingleton.sharedInstance.userFavorites()
                //datasSingleton.sharedInstance.downloadAvatar()
                
                //Je met à jour le user avec le token facebook.
                let realm = RLMRealm.defaultRealm()
                realm.beginWriteTransaction()
                var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                connectedUser.facebooktoken = self.fbSession!.accessTokenData.accessToken
                realm.commitWriteTransaction()
            }
            else {
                
//                dispatch_async(dispatch_get_main_queue(), {
//                    TAOverlay.hideOverlay()
//                })
                
                NSNotificationCenter.defaultCenter().postNotificationName("showError", object: nil, userInfo:["message":data["msg"]!])
            }
        }
    }
    
    func handleDidBecomeActive(){
        FBAppCall.handleDidBecomeActive();
    }
    
    func showAlert(msg:NSString!,result:AnyObject,error:NSError!) {
        var alertTitle:NSString!
        var alertMsg:NSString!;
        if (error != nil) {
            if((error.fberrorUserMessage != nil && FBSession.activeSession().isOpen) ) {
                alertTitle = "";
            }
            else{
                // Otherwise, use a general "connection problem" message.
                alertMsg = "Operation failed due to a connection problem, retry later.";
            }
        }
        else {
            var resultDict = result as! NSDictionary
            var postID:String = resultDict.valueForKey("id") as! String
            
            if(postID == "")
            {
                postID = resultDict.valueForKey("postId") as! String
            }
            else
            {
                alertMsg = "Post ID : \(postID)"
            }
            
            alertTitle = "Success"
        }
        
        if(alertTitle != ""){
            
            //var dictResult:NSDictonary = result as NSDictionary
            var alertObj:UIAlertView = UIAlertView(title:alertTitle as String, message:alertMsg as String, delegate:nil, cancelButtonTitle:"Ok")
            alertObj.show()
        }
    }
    
    func post(useToken:Bool = false,type:String,params : NSData, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
//        if(!Reachability.isConnectedToNetwork()){
//            var refreshAlert = UIAlertController(title: "Erreur", message: "Veuillez vérifier votre connection internet.", preferredStyle: UIAlertControllerStyle.Alert)
//            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
//                println("Ok")
//            }))
//            
//            presentViewController(refreshAlert, animated: true, completion: nil)
//            return
//        }
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = params
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    
                    //Login ok
                    if(type == "registerFacebookUser"){
                        if let success = parseJSON["sessid"] as? String {
                            //println("Succes:")
                            var data:[String:String] = ["token":parseJSON["token"] as! String]
                            
                            //Création de l'objet user
                            if let user = parseJSON["user"] as? NSDictionary {
                                
                                let realm = RLMRealm.defaultRealm()
                                realm.beginWriteTransaction()
                                UserModel.createOrUpdateInDefaultRealmWithObject(user)
                                println(user)
                                realm.commitWriteTransaction()
                                
                                //Au login du user, je lui met le token.
                                realm.beginWriteTransaction()
                                var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                                connectedUser.token = parseJSON["token"] as! String
                                realm.commitWriteTransaction()
                            }
                            
                            postCompleted(succeeded: true, data: data)
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Une erreur est survenue, verifiez vos champs."])
                        }
                    }
                    return
                }
                else {

                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr)")
                        postCompleted(succeeded: false, data: ["msg":jsonStr as! String])

                }
            }
        })
        
        task.resume()
    }
    
}

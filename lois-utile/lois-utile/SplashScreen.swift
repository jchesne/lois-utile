//
//  SplashScreen.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 31/12/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import Foundation
//import SwifteriOS

protocol finishLoadingDelegate{
    func loadingFinish()
}

class SplashScreen: UIViewController {
    
    var delegate:finishLoadingDelegate?

    @IBOutlet weak var textCitation: UILabel!
    @IBOutlet weak var auteurCitation: UILabel!
    
    let dataSingleton = datasSingleton.sharedInstance
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        GMDCircleLoader.setOnView(self.view, withTitle: "", animated: true)
        selectRandomCitation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textCitation.alpha = 0
        self.auteurCitation.alpha = 0
        
        if Reachability.isConnectedToNetwork() {
            initUser()
        }
        else{
            NSNotificationCenter.defaultCenter().postNotificationName("noInternetAlert", object: nil)
            var timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("cacheMe"), userInfo: nil, repeats: false)
        }
    }
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        selectRandomCitation()
//    }

    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "finishInitUser",
            name: "finishInitUser", object: nil)
    }

    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "finishInitUser", object: nil)
    }

    func finishInitUser(){
        dispatch_async(dispatch_get_main_queue(), {
             self.cacheMe()
        })
       
//        let userInfo:Dictionary<String,String!> = notification.userInfo as Dictionary<String,String!>
//        let messageString = userInfo["message"]
//        
//        var refreshAlert = UIAlertController(title: "Erreur", message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
//        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
//            println("Ok")
//        }))
//        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    /* Fonction qui initialise l'utilisateur de l'application */
    func initUser(){
        
        var connectedUser = UserModel.getUser()
        if(connectedUser != nil){
            // Je regarde de quel type a été la connection de l'utilisateur en fonction de si il a un mdp ou si le facebook token existe.
            
            // L'utilisateur c'est connecté via user/password.
            if(connectedUser?.password != ""){
                
                //Avant de faire une reconnection, je regenere un token que je vais utiliser temporairement ici
                dataSingleton.getToken({
                    dispatch_async(dispatch_get_main_queue(), {
                        // Je connecte l'utilisateur en utilisant le user/password dans la base.
                        self.dataSingleton.login_user(connectedUser!.mail, password: connectedUser!.password,useToken: true)
                    })
                })
                
            }
            else if(connectedUser?.facebooktoken != ""){
                //Avant de faire une reconnection, je regenere un token que je vais utiliser temporairement ici
                // Je connecte l'utilisateur en utilisant facebook.
                dataSingleton.getToken({
                    dispatch_async(dispatch_get_main_queue(), {
                        // Je connecte l'utilisateur en utilisant le user/password dans la base.
                        FB.login({
                            FB.getInfo(loginAfterGettingInfo: true, useToken: true)
                        });
                    })
                })
            }
            else if(connectedUser?.twittertoken != ""){
                dataSingleton.getToken({
                    dispatch_async(dispatch_get_main_queue(), {
                        TwitterClass.initSession(userInit: true,callback: {
                            TwitterClass.login(useToken: true)
                            })
                        })
                    })
            }
        }
        else{
            //Je n'ai pas d'utilisateur en local, je ne fais rien
            var timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("cacheMe"), userInfo: nil, repeats: false)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

//    @IBAction func test(sender: AnyObject) {
//        cacheMe()
//    }
    
    func cacheMe(){
        self.delegate?.loadingFinish()
        
        UIView.animateWithDuration(0.4, animations: {
            self.view.alpha = 0.0
            }, completion: {
                (finished: Bool) -> Void in
                self.dismissViewControllerAnimated(true) { () -> Void in
                }
        })
    }

    
    func selectRandomCitation(){
        var random = arc4random_uniform(UInt32(citations.count))
        
        println(random)
        var selectedCitation = citations[Int(random)]

        
        self.textCitation.text = selectedCitation["text"]
        self.auteurCitation.text = selectedCitation["auteur"]
        
        
    
        UIView.animateWithDuration(1, animations: {
            self.textCitation.alpha = 1
            self.auteurCitation.alpha = 1
        })
    }
}

import UIKit

class ColumnHeaderView: UICollectionReusableView
{
    
    var categorie: UILabel!
    var nombre: UILabel!
    
    convenience init() {
        self.init()
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        
    }
    
    func setup(){
        let categorieFrame = CGRect(x: 25, y: 30, width: frame.size.width, height: frame.size.height/3)
        categorie = UILabel(frame: categorieFrame)
        categorie.textColor = UIColor(red: 95/255, green: 87/255, blue: 79/255, alpha: 1.0)
        categorie.font = UIFont(name: "Helvetica-Light", size: 18)
        self.addSubview(categorie)
        
        let textFrame = CGRect(x: 25, y: 25, width: frame.size.width - 20, height: frame.size.height - 30 )
        nombre = UILabel(frame: textFrame)
        nombre.textColor = UIColor(red: 95/255, green: 87/255, blue: 79/255, alpha: 0.5)
        nombre.font = UIFont(name: "Helvetica-Light", size: 14)
        nombre.numberOfLines = 6
        self.addSubview(nombre)
    }
}
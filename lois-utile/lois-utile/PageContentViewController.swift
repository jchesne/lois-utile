//
//  ViewController.swift
//  Swift Pages
//
//  Created by Clayton McIlrath on 6/19/14.
//  Copyright (c) 2014 Unicorn Ltd. All rights reserved.
//

import UIKit
import Realm
import SwifteriOS
import SwiftyJSON

class PageContentViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate, finishCreationCommentaireDelegate, actionCommentaireDelegate, actionLoisCellDelegate{

    

    var swifter: Swifter
    
    required init(coder aDecoder: NSCoder) {
        self.swifter = Swifter(consumerKey: "JoM4bx9uBGMoRpLIc5owK84ng", consumerSecret: "5F1c2U9o3iuNZ4RLLgilvjHWkHLwRbZwzbvlHEWsWRX1qQd8dp")
        super.init(coder: aDecoder)
    }
    
    var pageIndex : Int = 0
    var titleText : String = ""
    
    var detailLoisModel: Lois!
    
    var favorisImage = "favorites"
    var favorisText = "Ajouter en favoris"
    var clickedIndex:NSIndexPath = NSIndexPath(forRow: 5, inSection: 0)
    var scrollPosition:CGPoint!

    var ressourcesArray:[String] = []
    
    var etat_vote = VotesModel()
    var etat_favoris = Favoris()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var liste_commentaires = LoisComments.allObjects()
    var liste_top_commentaires = LoisComments.allObjects()

    
    var display_commentaires = true
    
    var notificationToken: RLMNotificationToken?
    
    var current_commentaire_action_index:NSIndexPath!
    
    func creationCommentaireFinish(){
        println("Je fais appel à la récupération des commentaires.")
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.getComments()
        })
    }

    override func viewDidLoad() {
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        
        super.viewDidLoad()
        
        self.liste_commentaires = LoisComments.objectsWhere("nid = '\(detailLoisModel.nid)'").sortedResultsUsingProperty("index", ascending: true)
        let sortProperties = [RLMSortDescriptor(property: "vote_plus", ascending: false), RLMSortDescriptor(property: "date_creation", ascending: false)]
        self.liste_top_commentaires = LoisComments.objectsWhere("nid = '\(detailLoisModel.nid)'").sortedResultsUsingDescriptors(sortProperties)
        
        //Je charge les commentaires de la lois.
        getComments()
        
        notificationToken = RLMRealm.defaultRealm().addNotificationBlock { note, realm in
            self.tableView.reloadData()
        }
        

        self.tableView.delaysContentTouches = false
        
        //@TODO: gérer les ressources
        self.ressourcesArray = detailLoisModel.ressources.componentsSeparatedByString(",")
        
        self.etat_vote = VotesModel.lois_etat_vote(self.detailLoisModel)
        self.etat_favoris = Favoris.lois_etat_favoris(self.detailLoisModel)
    }
    
    override func viewDidAppear(animated: Bool) {
        //println("passe")
            //tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Bottom, animated: true)
    }
    
//    override func viewDidDisappear(animated: Bool) {
//        println(self.tableView.contentOffset)
//    }
    
    func getComments(){
        self.post(useToken:false,type:"getComments",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/free/contents/getComments") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                //self.tableView.reloadData()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
//    func recupereVote()->VotesModel?{
//        if VotesModel.objectsWhere("id_lois = '\(detailLoisModel.nid)'").count > 0 {
//            return VotesModel.objectsWhere("id_lois = '\(detailLoisModel.nid)'").firstObject() as? VotesModel
//        }
//        else{
//            return nil
//        }
//    }
    
//    func recupereFavoris()->Favoris?{
//        if Favoris.objectsWhere("lois.nid = '\(detailLoisModel.nid)'").count > 0 {
//            return Favoris.objectsWhere("lois.nid = '\(detailLoisModel.nid)'").firstObject() as? Favoris
//        }
//        else{
//            return nil
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if(section == 0){
           return 4
        }
        else if(section == 1){
            if(display_commentaires && (Int(liste_top_commentaires.count) > 0) ){
                if((liste_top_commentaires.firstObject() as! LoisComments).vote_plus != 0){
                    if(Int(liste_top_commentaires.count) > 1){
                        if((liste_top_commentaires.objectAtIndex(1) as! LoisComments).vote_plus != 0){
                            return 2
                        }
                        else{
                            return 1
                        }
                    }
                    else{
                        return 1
                    }
                }
                else{
                    return 0
                }
            }
            else{
                return 0
            }
        }
        else{
            if(display_commentaires && (Int(liste_commentaires.count) > 0)){
                return Int(liste_commentaires.count)
            }
            else{
                return 0
            }
        }
        // Les 7 premiers body + social
        //Header Commentaire top
        //contenu commentaires top
        //Header commentaires
        //contenu commentaires
    }
    
    func ajoutLoisFavoris(){
        ajout_delete_favoris()
    }
    
    /*
    Fonction de delegate depuis detailLoisBodyCell.
    */
    func voteLois(){
        self.scrollPosition = self.tableView.contentOffset
        change_vote(1)
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            

        if(indexPath.row == 0){
            let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiBodyCell", forIndexPath: indexPath) as! detailLoisBodyCell
            cell.titreLabel.text = detailLoisModel.title
            cell.bodyLabel.text = detailLoisModel.body
            cell.categorieLabel.text = detailLoisModel.categorie_name
            cell.dateAjoutLabel.text = "ajouté le \(detailLoisModel.changed) dans"
            cell.topButton.setTitle("\(detailLoisModel.vote_plus)", forState: UIControlState.Normal)
            cell.nombre_commentaires.setTitle("\(detailLoisModel.comments_count)", forState: UIControlState.Normal)
        
            if(self.etat_favoris != nil){
                //On affiche l'étoile orange
                cell.isFavorite.setImage(UIImage(named: "favoritesOn"), forState: UIControlState.Normal)
            }
            else{
                //On affiche l'étoile grise
                cell.isFavorite.setImage(UIImage(named: "favorites"), forState: UIControlState.Normal)
            }
            
            cell.delegate = self
            
            cell.layoutIfNeeded()
            
            return cell
        }
        else if(indexPath.row == 1){
            let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiTagsCell", forIndexPath: indexPath) as! detailLoisTagsCell

            return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiActionCell", forIndexPath: indexPath) as! detailLoisActionCell
            
            var ressourcesCount = 0
            if(indexPath.row == 2){
                    if(detailLoisModel.ressources != ""){
                        ressourcesCount = detailLoisModel.ressources.componentsSeparatedByString(",").count
                        cell.selectionStyle = UITableViewCellSelectionStyle.Default
                        cell.userInteractionEnabled = true
                        cell.alpha = 1
                    }
                    else{
                        cell.selectionStyle = .None
                        cell.userInteractionEnabled = false
                        cell.alpha = 0.5
                    }
            }
            else{
                cell.selectionStyle = UITableViewCellSelectionStyle.Default
                cell.userInteractionEnabled = true
                cell.alpha = 1
            }
            

            
            
            switch indexPath.row {
            case 2:
                cell.actionLabel.text = "Ressources (\(ressourcesCount))"
                cell.actionImage.image = UIImage(named: "ressources")
            case 3:
                cell.actionLabel.text = "Commenter"
                cell.actionImage.image = UIImage(named: "compose")

            default:
                cell.actionLabel.text = "Partager"
                cell.actionImage.image = UIImage(named: "upload")
            }
            
            return cell
        }
        }
        //Top commentaires
        else if(indexPath.section == 1){
            
            var loisComment = (liste_top_commentaires.objectAtIndex(UInt(indexPath.row)) as! LoisComments)
            
            let comment_top_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCell", forIndexPath: indexPath) as! CommentCell
            comment_top_cell.bodyCommentLabel.text = loisComment.body
            comment_top_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
            
            comment_top_cell.delegate = self
            comment_top_cell.index = indexPath
            
            let URL = NSURL(string:loisComment.avatar)!
            comment_top_cell.avatar.hnk_setImageFromURL(URL)
            
            return comment_top_cell
        }
        //Tous les commentaires
        else{
            var loisComment = (liste_commentaires.objectAtIndex(UInt(indexPath.row)) as! LoisComments)
            if(loisComment.parent != "0"){
                let comment_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCellChildren", forIndexPath: indexPath) as! CommentCellChildren
                comment_cell.bodyCommentLabel.text = loisComment.body
                comment_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
                
                comment_cell.delegate = self
                comment_cell.index = indexPath
                
                let URL = NSURL(string:loisComment.avatar)!
                comment_cell.avatar.hnk_setImageFromURL(URL)
                
                return comment_cell
            }
            else{
                let comment_cell = self.tableView.dequeueReusableCellWithIdentifier("DetailLoiCommentCell", forIndexPath: indexPath) as! CommentCell
                comment_cell.bodyCommentLabel.text = loisComment.body
                comment_cell.auteurLabel.text = "\(loisComment.prenom_nom)" + ", le \(loisComment.date_creation)"
                
                comment_cell.delegate = self
                comment_cell.index = indexPath
                
                let URL = NSURL(string:loisComment.avatar)!
                comment_cell.avatar.hnk_setImageFromURL(URL)

                return comment_cell
            }
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 44
        }
        else{
            return 124
        }
    }

    
    func repondreCommentaire(index:NSIndexPath){
        self.current_commentaire_action_index = index
        performSegueWithIdentifier("showRepondreComment", sender: self)
    }
    
    func notePlusCommentaire(index:NSIndexPath){
        //appel post noter plus
    }
    
    func noteMoinsCommentaire(index:NSIndexPath){
        //appel post noter moins
    }
    
    func signaler(index:NSIndexPath){
        //alert signaler un abus ou autre.
        
        var refreshAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        refreshAlert.addAction(UIAlertAction(title: "Copier le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Copie du commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Partager le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Partager le commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Signaler le commentaire", style: .Default, handler: { (action: UIAlertAction!) in
            //Signaler le commentaire
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as! HeaderCell

        switch (section) {
        case 0:
            headerCell.headerLabel.text = "";
            //return sectionHeaderView
        case 1:
            headerCell.headerLabel.text = "Meilleurs commentaires";
            //return sectionHeaderView
        case 2:
            headerCell.headerLabel.text = "\(liste_commentaires.count) commentaires";
            //return sectionHeaderView
        default:
            headerCell.headerLabel.text = "";
        }
        
        return headerCell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return CGFloat(0.0)
        }
        else{
            return CGFloat(44)
        }
    }
    
    /*
        Fonction qui gère le click sur les réseaux sociaux et la mise en favoris.
    */
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Pour l'instant je ne gère le click que sur la 1ere section.
        if(indexPath.section == 0){
            self.scrollPosition = self.tableView.contentOffset
            
            self.performSegueWithIdentifier("showListeRessources", sender: self)
            
//            switch indexPath.row {
//            case 1:
//                shareSocial()
//            case 2:
//                ajout_delete_favoris()
//            case 4:
//                change_vote(1)
//            case 5:
//                change_vote(-1)
//            case 6:
//                
//                self.performSegueWithIdentifier("showListeRessources", sender: self)
//            default:
//                println("Click sur le texte.")
//            }
        }
    }
    
    func shareSocial(){
        var options = (TAOverlayOptions.AllowUserInteraction | TAOverlayOptions.AutoHide | TAOverlayOptions.OverlayTypeSuccess | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Success",options: options)

        //@Todo: Afficher une jolie fenêtre pour partager sur les réseaux sociaux, mail, copy url, copy message.
        //FB.shareContent()
        
//        let failureHandler: ((NSError) -> Void) = {
//            error in
//            
//            self.alertWithTitle("Error", message: error.localizedDescription)
//        }
        

        // save session
//        let accessToken = swifter.client.credential.accessToken {
//            let key = accessToken.key // String
//            let secret = accessToken.secret // String
//            // save the key and secret to Keychain
//        }
//        
//        // load session
//        let key = ... // load the key from Keychain
//        let secret = ... // load the secret from Keychain
//        let accessToken = SwifterCredential.OAuthAccessToken(key: key, secret: secret)
//        let credential = SwifterCredential(accessToken: accessToken)
//        
//        let swifter = Swifter(consumerKey: "", consumerSecret: "")
//        swifter.client.credential = credential
//         Si je n'ai pas de credential ça veut dire que je ne suis pas loggé sur twitter
//        if(swifter.client.credential == nil){
//            
//        }
//        else{
//            swifter.postStatusUpdate("Hello, world", inReplyToStatusID: nil, lat: nil, long: nil, placeID: nil, displayCoordinates: nil, trimUser: nil, success: {
//                (status: [String : SwifteriOS.JSONValue]?) in
//                
//                println("toto")
//                
//                }, failure: {
//                    (error: NSError) in
//                    
//                    println(error.localizedDescription)
//            })
//        }
        
        
    }
    
    func alertWithTitle(title: String, message: String) {
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func change_vote(etat:Int){
        // Ici j'appelle le WS qui va créer/supprimer ou modifier le vote.
        
        // Les anonymes et les connecté peuvent voter, mais ça fait appel à 2 WS différents, en fonction du status de connection je fais appel à l'un ou l'autre.
        var useToken = false
        var url = "\(appDelegate.url)/api/v1/fr/free/votingapi/set_votes"
        if((UserModel.getUser()) != nil){
            useToken = true
            url = "\(appDelegate.url)/api/v1/fr/votingapi/set_votes"
        }
        
        self.post(useToken: useToken,type:"setVotes",params: ["entity_id":self.detailLoisModel.nid, "value":String(etat), "type":"node"], url: url) { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    
                    /*
                    Creation / Modification / Suppression du model Vote.
                    
                    Le model Lois a été modifié dans la réponse du WS.
                    */
                    if(self.etat_vote != nil){
                        if(self.etat_vote.etat_vote == etat){
                            //self.etat_vote = recupereVote()
                            realm.deleteObject(self.etat_vote)
                            self.etat_vote = nil
                            println("Je supprime le vote")
                        }
                        else{
                            self.etat_vote.etat_vote = etat
                            println("Je change le vote.")
                        }
                    }
                    else{
                        //On a encore jamais voté, je le crée.
                        var vote = VotesModel()
                        vote.etat_vote = etat
                        vote.id_lois = self.detailLoisModel.nid
                        
                        self.etat_vote = vote
                        
                        realm.addObject(self.etat_vote)
                        println("Je crée le vote.")
                    }
                    
                    // Je met à jour le node avec la valeur des nouveaux votes.
                    
                    realm.commitWriteTransaction()
                    //self.tableView.reloadData()
                    var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                   // self.tableView.contentOffset = self.scrollPosition
                    
                })
                
            }
            else {
                var alert = UIAlertView(title: "Erreur", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
        

    }

    
    func ajout_delete_favoris(){
        // Si le favoris est déjà présent je le supprime.
        if(self.etat_favoris != nil){
            
            self.post(useToken: true,type:"deleteFavorites",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/contents/deleteFavorites") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let realm = RLMRealm.defaultRealm()
                        realm.beginWriteTransaction()
                        
                        realm.deleteObject(self.etat_favoris)
                        self.etat_favoris = nil
                        println("Suppression du favoris.")
                        
                        realm.commitWriteTransaction()
                        
                         var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                        self.tableView.reloadData()
                    })
                    
                }
                else {
                    var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
        
        }
        // Le favoris n'est pas présent, je le rajoute.
        else{
           //On a encore jamais mis en favoris, je le crée.
            //@TOD0: Vérifier la connection internet.
            self.post(useToken: true,type:"addFavorites",params: ["nid":self.detailLoisModel.nid], url: "\(appDelegate.url)/api/v1/fr/contents/addFavorites") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let realm = RLMRealm.defaultRealm()
                        realm.beginWriteTransaction()

                        var favoris = Favoris()
                        var dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "dd/MM/YYYY"
                        //DateFormatter.stringFromDate(NSDate())
                        favoris.date_ajout = data["date_ajout"]!
                        favoris.lois = self.detailLoisModel
                        self.etat_favoris = favoris
                        
                        realm.addObject(self.etat_favoris)
                        
                        realm.commitWriteTransaction()
                        println("Création du favoris.")
                        
                        self.tableView.reloadData()
                        
                        var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("update"), userInfo: nil, repeats: false)
                    })
                }
                else {
                    var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
        }
    }

    func update() {
     //   if(self.scrollPosition != nil){
          //  self.tableView.contentOffset = self.scrollPosition
       // }
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showListeRessources" {
            let listeRessourcesTableViewController = segue.destinationViewController as! ListeRessourcesTableViewController
            listeRessourcesTableViewController.ressourcesArray = self.ressourcesArray
        }
        else if(segue.identifier == "showRepondreComment"){
                
                
                let createCommentViewController = segue.destinationViewController as! CreationCommentaireViewController
                createCommentViewController.delegate = self
                createCommentViewController.nid = detailLoisModel.nid

                
                var loisComment:LoisComments!
                
                // On répond à un commentaire top
                if(self.current_commentaire_action_index.section == 1){
                    loisComment = (liste_top_commentaires.objectAtIndex(UInt(self.current_commentaire_action_index.row)) as! LoisComments)
                    
                }
                // On répond aux autres commentaires
                else{
                    loisComment = (liste_commentaires.objectAtIndex(UInt(self.current_commentaire_action_index.row)) as! LoisComments)
                }
                
                createCommentViewController.pid = loisComment.cid
                createCommentViewController.repondre_a = loisComment.prenom_nom
            }
    }
    
    
    func post(useToken:Bool = false,type:String,params : Dictionary<String, String>, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        
        if(!Reachability.isConnectedToNetwork()){
//            var refreshAlert = UIAlertController(title: "Erreur", message: "Veuillez vérifier votre connection internet.", preferredStyle: UIAlertControllerStyle.Alert)
//            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
//                println("Ok")
//            }))
//            presentViewController(refreshAlert, animated: true, completion: nil)
//            return
            //Je n'ai pas internet, en fonction de l'action souhaité j'affiche ou non l'alerte.
            var array:[String] = ["addFavorites" ,"deleteFavorites" ,"setVotes"]
            var trouve = false
            for value in array{
                if(type == value){
                    trouve = true
                    break
                }
            }
            
            if(trouve){
                NSNotificationCenter.defaultCenter().postNotificationName("noInternetAlert", object: nil)
                return
            }
            else{
                NSNotificationCenter.defaultCenter().postNotificationName("noInternet", object: nil)
                return
            }
        }
        
        if(useToken == true && UserModel.getUser() == nil){
            var refreshAlert = UIAlertController(title: nil, message: "Vous devez vous connecter pour effectuer cette opération.", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Connection", style: .Default, handler: { (action: UIAlertAction!) in
                println("Je lance la connection")
                
                self.performSegueWithIdentifier("showCreationCompteAlert", sender: self)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                println("J'annule la fenetre")
            }))
            presentViewController(refreshAlert, animated: true, completion: nil)
            return
        }
        
        if(type != "getComments"){
            var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
            TAOverlay.showOverlayWithLabel(nil,options: options)
        }
        
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
       
        
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Get COmments
                    if(type == "getComments"){
                        if let commentaires = parseJSON["comments"] as? [NSDictionary] {
                            
                            let realm = RLMRealm.defaultRealm()
                            
                            // Ici je crée les catégories / lois en utilisant Realm qui va instancier et mettre à jour automatiquement mes objets.
                            realm.beginWriteTransaction()
                            for commentaire in commentaires {
                                LoisComments.createOrUpdateInDefaultRealmWithObject(commentaire)
                            }
                            realm.commitWriteTransaction()
                            
                            self.display_commentaires = true
                            
                            postCompleted(succeeded: true, data:["msg":"Contenu"])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Addresse Email déjà utilisée."])
                        }
                    }
                    // Add favoris
                    if(type == "addFavorites"){
                        if let success = parseJSON["success"] as? String {
                            postCompleted(succeeded: true, data:["date_ajout":success])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans lajout du favoris."])
                        }
                    }
                    else if(type == "deleteFavorites"){
                        if let success = parseJSON["success"] as? Bool {
                            postCompleted(succeeded: true, data:["":""])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans la suppression du favoris."])
                        }
                    }
                    else if(type == "setVotes"){
                        if let success = parseJSON["success"] as? Bool {
                            let action = parseJSON["action"] as? String
                            
                            if(action == "setvote"){
                                // je dispatche dans le thread principal pour pouvoir mettre à jour mon objet qui a été crée dans le thread principal.
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    let realm = RLMRealm.defaultRealm()
                                    realm.beginWriteTransaction()
                                    //Je set le nombre de vote pour la lois courante.
                                    if let votes = parseJSON["votes"] as? [NSDictionary]{
                                        let json = JSON(parseJSON)
                                        self.detailLoisModel.vote_plus = 0
                                      //  self.detailLoisModel.vote_moins = 0
                                        for (index: String, subJson: SwiftyJSON.JSON) in json["votes"] {
                                            if(subJson["function"] == "option-1"){
                                                var value = subJson["value"].string!
                                                self.detailLoisModel.vote_plus = value.toInt()!
                                            }
                                        }
                                    }
                                    realm.commitWriteTransaction()
                                })
                                
                                postCompleted(succeeded: true, data:["msg":"Setting du vote ok"])
                            }
                            else{
                                postCompleted(succeeded: true, data:["msg":"Suppression du vote ok"])
                            }
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans la suppression du favoris."])
                        }
                    }
                    return
                }
                else {
                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr)")
                        postCompleted(succeeded: false, data: ["msg":"Erreur."])
                }
            }
        })
        
        task.resume()
    }
}


//
//  HeaderCellPageProfilTableViewCell.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 05/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class HeaderCellPageProfilTableViewCell: UITableViewCell {

    @IBOutlet weak var button1: switchContentProfilButton!
    @IBOutlet weak var button2: switchContentProfilButton!
    @IBOutlet weak var button3: switchContentProfilButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

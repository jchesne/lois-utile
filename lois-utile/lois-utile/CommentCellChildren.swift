//
//  CommentCellChildren.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 08/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit



class CommentCellChildren: UITableViewCell {

    @IBOutlet weak var auteurLabel: UILabel!
    @IBOutlet weak var bodyCommentLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    var delegate:actionCommentaireDelegate?
    
    var index:NSIndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    @IBAction func repondreCommentaire(sender: AnyObject) {
        self.delegate?.repondreCommentaire(index)
    }
    
    @IBAction func notePlus(sender: UIButton) {
        self.delegate?.notePlusCommentaire(index)
    }
    
    
//    @IBAction func noteMoins(sender: UIButton) {
//        self.delegate?.noteMoinsCommentaire(index)
//    }
    
    @IBAction func signalerCommentaire(sender: UIButton) {
        self.delegate?.signaler(index)
    }
    
    override func prepareForReuse() {
        avatar.hnk_cancelSetImage()
        avatar.image = nil
    }
}

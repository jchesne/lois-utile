//
//  utils.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 02/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import Foundation
import UIKit


class utils: UIViewController{

    func share()->UIAlertController{
        
        var alert = UIAlertController(title: "Partage", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Facebook", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            println("Partage sur Facebook")
        }))
        alert.addAction(UIAlertAction(title: "Twitter", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            println("Partage sur Twitter")
        }))
        return alert
    }
    
}
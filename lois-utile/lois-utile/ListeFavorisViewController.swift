//
//  ListeFavorisViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 14/11/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

class ListeFavorisViewController: UITableViewController {

    @IBOutlet weak var user_avatar_icon: UIButton!

    var arrayOfId:[String] = []
    var liste_Favoris = Favoris.allObjects().sortedResultsUsingProperty("date_ajout", ascending: false)
    let directoryURLString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
    let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL
    
    //@IBOutlet var favorisTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.addPullToRefresh({ [weak self] in
            // refresh code
            
            self?.tableView.reloadData()
            self?.tableView.stopPullToRefresh()
        })

    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        self.tableView.fixedPullToRefreshViewForDidScroll()
    }
    
    override func viewWillAppear(animated: Bool) {
        
//        var id_of_favorites = [Int]()
//        for favoris in Favoris.allObjects(){
//            var item = favoris as Favoris
//            id_of_favorites[item.nid.toInt()!] = item.nid.toInt()!
//        }
//
//        let predicate = NSPredicate(format: "nid IN %@", id_of_favorites)
   //     self.liste_Node_Favoris = Lois.objectsWithPredicate(predicate)
        
        if(UserModel.getUser() != nil){
            let filemanager = NSFileManager.defaultManager()
            var fichier = self.directoryURLString.stringByAppendingPathComponent("avatar/user_avatar.jpg")
            if(filemanager.fileExistsAtPath(fichier)){
                var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
                var imageData :NSData = NSData(contentsOfURL:nsurl)!
                self.user_avatar_icon.setImage(UIImage(data: imageData), forState: UIControlState.Normal)
                
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self

        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        update_avatar()
    }
    
    func update_avatar(){
        if(UserModel.getUser() != nil){
            let filemanager = NSFileManager.defaultManager()
            var fichier = self.directoryURLString.stringByAppendingPathComponent("avatar/user_avatar.jpg")
            if(filemanager.fileExistsAtPath(fichier)){
                var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
                var imageData :NSData = NSData(contentsOfURL:nsurl)!
                
                let avatar_image = UIImage(data: imageData)
                
                if(self.user_avatar_icon.imageForState(UIControlState.Normal) == nil){
                    UIView.transitionWithView(self.user_avatar_icon,
                        duration:1,
                        options: UIViewAnimationOptions.TransitionCrossDissolve,
                        animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                        completion: nil)
                }
                else{
                    let data1:NSData = UIImagePNGRepresentation(self.user_avatar_icon.imageForState(UIControlState.Normal))
                    let data2:NSData = UIImagePNGRepresentation(avatar_image)
                    
                    if(!data1.isEqual(data2)){
                        UIView.transitionWithView(self.user_avatar_icon,
                            duration:1,
                            options: UIViewAnimationOptions.TransitionCrossDissolve,
                            animations: { self.user_avatar_icon.setImage(avatar_image, forState: UIControlState.Normal) },
                            completion: nil)
                    }
                }
            }
        }
        else{
            self.user_avatar_icon.setImage(UIImage(named: "user"), forState: UIControlState.Normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(Int(liste_Favoris.count) > 0){
            self.tableView.backgroundView = nil;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
            return Int(liste_Favoris.count)
        }
        else{
            var message:UILabel = UILabel()
            message.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
            
            message.text = "Vous n'avez aucun favoris.";
            message.textColor = UIColor.blackColor()
            message.numberOfLines = 0;
            message.textAlignment = NSTextAlignment.Center;
            message.font = UIFont(name: "Helvetica-Light", size: 14.0)
            message.sizeToFit()
            
            self.tableView.backgroundView = message;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
            
            return 0
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FavorisCell", forIndexPath: indexPath) as! FavorisCell
        
        var currentFavoris = liste_Favoris[UInt(indexPath.row)] as! Favoris
        var lois = (currentFavoris).lois
        
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        var date_converted = NSDate(timeIntervalSince1970: (currentFavoris.date_ajout as NSString).doubleValue)
        let date = dateFormatter.stringFromDate(date_converted)
        
        cell.titreLabel.text = lois.title
        cell.bodyLabel.text = lois.body
        cell.ajoutDateLabel.text = "\(lois.categorie_name) • Ajouté le \(date)"
        
        cell.backgroundColor = (indexPath.row % 2 == 0) ? UIColor(red: 234.0/255.0, green: 232.0/255.0, blue: 230.0/255.0, alpha: 1) : UIColor(red: 244.0/255.0, green: 243.0/255.0, blue: 242.0/255.0, alpha: 1)
        
        return cell
    }
//    
//    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        self.performSegueWithIdentifier("favorisToDetail", sender: tableView)
//    }
//    
    
    @IBAction func connectionButton(sender: UIButton) {
        if((UserModel.getUser()) != nil){
            self.performSegueWithIdentifier("showCompteDetail", sender: self)
        }
        else{
            self.performSegueWithIdentifier("showCreationCompte", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "favorisToDetail" {
            let indexPath: AnyObject? = tableView.indexPathForSelectedRow()
            
            
            let detailLoisViewController = segue.destinationViewController as! UILoisPageViewController
            detailLoisViewController.currentIndex = indexPath!.row!
            
            var predicateTitleArray:[NSPredicate] = []


            
            // Je boucle parmis les Favoris pour récupérer les nid
            var array_of_id = [String]()
            
            let arrayOfRecords = RLMArray(objectClassName: "Lois")

            
            for favori in self.liste_Favoris{
                var item = (favori as! Favoris).lois
                 arrayOfRecords.addObject(item)
            }

            detailLoisViewController.displayFavorites = true
            detailLoisViewController.arrayOfRecords = arrayOfRecords
        }
    }
}

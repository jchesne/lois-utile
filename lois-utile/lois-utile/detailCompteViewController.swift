//
//  detailCompteViewController.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 30/01/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit
import Realm

class detailCompteViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as? NSURL
    
    var currentIndex = 1
    @IBOutlet weak var tableView: UITableView!
    
    var liste_comments = UserComments.allObjects().sortedResultsUsingProperty("date_creation", ascending: false)
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var emailImage = UIImage(named: "email")
        var cogImage = UIImage(named: "cog")
        var paramButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        paramButton.setImage(cogImage, forState: UIControlState.Normal)
        paramButton.setImage(cogImage?.imageWithColor(UIColor.blackColor()), forState: UIControlState.Highlighted)
        paramButton.addTarget(self, action: Selector("showParams"), forControlEvents:  UIControlEvents.TouchUpInside)
        var itemParam = UIBarButtonItem(customView: paramButton)
        
        var emailButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        emailButton.setImage(emailImage, forState: UIControlState.Normal)
        emailButton.setImage(emailImage?.imageWithColor(UIColor.blackColor()), forState: UIControlState.Highlighted)
        emailButton.addTarget(self, action: Selector("showEmails"), forControlEvents:  UIControlEvents.TouchUpInside)
        var itemEmail = UIBarButtonItem(customView: emailButton)
        
        self.navigationItem.rightBarButtonItems = [itemParam,itemEmail]
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 450.0
        
        self.tableView.addPullToRefresh({ [weak self] in
            println("Faire appel au code de mise à jour des lois.")
            // refresh code
            //
            //            self?.tableView.reloadData()
            //            self?.tableView.stopPullToRefresh()
        })
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.tableView.fixedPullToRefreshViewForDidScroll()
    }
    
    override func viewWillAppear(animated: Bool) {
        // Je crée un listener qui lors de la deconection renvera sur la home.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "PreviousPage",
            name: "finishDestroyUser", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "changeData:",
            name: "changeData", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "backToPreviousPageHandler",
            name: "backToPreviousPage", object: nil)
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "changeData", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "finishDestroyUser", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "backToPreviousPage", object: nil)
    }
    
    func backToPreviousPageHandler(){
        self.navigationController?.popViewControllerAnimated(true)
    }

    func changeData(notification:NSNotification){
        let userInfo:Dictionary<String,Int!> = notification.userInfo as! Dictionary<String,Int!>
        let index:Int = userInfo["index"]!
        println("Nouveau Index \(index)")
        self.currentIndex = index
        self.tableView.reloadData()
    }

    
    func showEmails(){
        println("Show Emails")
    }
    
    func showParams(){
        self.performSegueWithIdentifier("showParams", sender: self)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(UserModel.getUser() == nil)
        {
            return 0
        }
        else{
            return 2
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCellPageProfilTableViewCell") as! HeaderCellPageProfilTableViewCell
        
        headerCell.button1.tag = 1
        headerCell.button1.labelText = "\(liste_comments.count)"
        headerCell.button2.tag = 2
        headerCell.button3.tag = 3
        
        if(currentIndex == 1){
            headerCell.button1.isActive = true
            headerCell.button2.isActive = false
            headerCell.button3.isActive = false
        }
        else if(currentIndex == 2){
            headerCell.button1.isActive = false
            headerCell.button2.isActive = true
            headerCell.button3.isActive = false
        }
        else if(currentIndex == 3){
            headerCell.button1.isActive = false
            headerCell.button2.isActive = false
            headerCell.button3.isActive = true
        }
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return CGFloat(0.0)
        }
        else{
            return CGFloat(40)
        }
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if(indexPath.section == 0){
//            return  108.0
//        }
//        else{
//            return 100.0
//        }
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        }
        else{
            // Ici en fonction de l'élément que j'ai choisi dans mon header je compte différement.
            // Commentaires
            // Lois soumises
            // Favoris ?
            // Likes ?
            if(self.currentIndex == 1){
                return Int(liste_comments.count)
            }
            else if(self.currentIndex == 2){
                return 2
            }
            else if(self.currentIndex == 3){
                return 2
            }
            
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        //J'affiche l'avatar
        if(indexPath.section == 0){
            let cell = self.tableView.dequeueReusableCellWithIdentifier("TopCellPageProfil", forIndexPath: indexPath) as! TopCellPageProfil
            
            cell.nomLabel.text = UserModel.getUser()!.prenom + " " + UserModel.getUser()!.nom
            
            //self.finalPath = self.directoryURL!.URLByAppendingPathComponent(folder+"/vignette.png")
            var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
            var imageData :NSData = NSData(contentsOfURL:nsurl)!
            cell.avatarImage.image = UIImage(data: imageData)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            //cell.userInteractionEnabled = false
            
            cell.layoutIfNeeded()
            
            return cell
        }
        else{
            
            if(self.currentIndex == 1){
                var userCommentObject = liste_comments[UInt(indexPath.row)] as! UserComments
                
                let user_commentCell = self.tableView.dequeueReusableCellWithIdentifier("UserCommentCell", forIndexPath: indexPath) as! UserCommentCell
                user_commentCell.bodyLabel.text = userCommentObject.body
                
                
                
                var dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM/YYYY"
                
                var date_converted = NSDate(timeIntervalSince1970: (userCommentObject.date_creation as NSString).doubleValue)
                let date = dateFormatter.stringFromDate(date_converted)
                
                let text = "Le \(date) dans \(userCommentObject.title_lois)"
                let attributedString = NSMutableAttributedString(string: text)
                user_commentCell.dateLabel.setText(text, afterInheritingLabelAttributesAndConfiguringWithBlock: { (NSMutableAttributedString) -> NSMutableAttributedString! in
                    var stringRange = (text as NSString).rangeOfString(userCommentObject.title_lois)
                    attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica-Light", size: 11.0)!, range: NSRange(location: 0,length: (text as NSString).length))
                    attributedString.addAttribute(kCTFontAttributeName as! String, value: UIFont(name: "Helvetica", size: 11.0)!, range: stringRange)
                    return attributedString
                })
                
                var nsurl = directoryURL!.URLByAppendingPathComponent("avatar/user_avatar.jpg")
                var imageData :NSData = NSData(contentsOfURL:nsurl)!
                user_commentCell.avatar.image = UIImage(data: imageData)
                
                user_commentCell.layoutMargins = UIEdgeInsetsZero;
                user_commentCell.preservesSuperviewLayoutMargins = false;
                
                return user_commentCell
            }

            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("listUserCommentToDetail", sender: self)
    }
    
    @IBAction func deconnectionButtonCliked(sender: AnyObject) {
        
        var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Deconnection",options: options)
        
        datasSingleton.sharedInstance.logout_user()
    }
    
    /*
    A la deconnection je renvois sur la page d'accueil.
    */
    func PreviousPage(){
        
        dispatch_async(dispatch_get_main_queue(), {
            TAOverlay.hideOverlay()
        })
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "listUserCommentToDetail" {
            if(self.currentIndex == 1){
                let indexPath: AnyObject? = self.tableView.indexPathForSelectedRow()
                let detailLoisViewController = segue.destinationViewController as! UILoisPageViewController
                detailLoisViewController.currentIndex = 0
                
                var selectedComment = liste_comments[UInt(indexPath!.row)] as! UserComments
                
                detailLoisViewController.ListeLoisParCategorie = Lois.objectsWhere("nid = '\(selectedComment.nid)'")
            }
            else if(self.currentIndex == 2){
                
            }
            else if(self.currentIndex == 3){
                
            }
            

            //            let detailLoisViewController = segue.destinationViewController as PageContentViewController
            //            let indexPath: AnyObject = tableView.indexPathForSelectedRow()!
            //            let selectedLois:Lois = liste_Lois[UInt(indexPath.row)] as Lois
            //            detailLoisViewController.detailLoisModel = selectedLois
        }
    }

}

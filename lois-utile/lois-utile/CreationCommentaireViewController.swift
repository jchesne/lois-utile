//
//  CreationCommentaireViewController.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 09/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import Foundation
import UIKit

protocol finishCreationCommentaireDelegate{
    func creationCommentaireFinish()
}

class CreationCommentaireViewController: UIViewController, UITextViewDelegate {
    
    var delegate:finishCreationCommentaireDelegate?
    
    var nid:String = ""
    
    var pid:String = ""
    var repondre_a:String = ""
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    @IBOutlet weak var repondreLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    var placeholderLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Entrez votre commentaire"
        placeholderLabel.font = UIFont.italicSystemFontOfSize(textView.font.pointSize)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPointMake(5, textView.font.pointSize / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.hidden = count(textView.text) != 0

        if(self.repondre_a != ""){
            self.repondreLabel.text = "Répondre à : \(self.repondre_a)"
            self.repondreLabel.hidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        textView.becomeFirstResponder()
    }
    
    func textViewDidChange(textView: UITextView) {
        placeholderLabel.hidden = count(textView.text) != 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func retour(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    @IBAction func creeCommentaire(sender: UIButton) {
        println("Je crée le commentaire")
        
        var params = [
            "nid":self.nid,
            "subject":"no subject",
            "comment_body":["und":[["value":self.textView.text]]]
        ]
        
        
        
        if(self.pid != ""){
            params["pid"] = self.pid
        }
        
        
        println(params)
        
        var err: NSError?
        var data = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        
        self.post(useToken: true,type:"postComment",params: data!, url: "\(self.appDelegate.url)/api/v1/fr/comment") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                println("Récupération des commentaires")
                dispatch_async(dispatch_get_main_queue(), {
                    self.navigationController?.popViewControllerAnimated(true)
                    self.delegate?.creationCommentaireFinish()
                })
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                alert.title = "Erreur."
                alert.message = data["msg"]
                // Move to the UI thread
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    func post(useToken:Bool = false,type:String,params : NSData, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        if(!Reachability.isConnectedToNetwork()){
            var refreshAlert = UIAlertController(title: "Erreur", message: "Veuillez vérifier votre connection internet.", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                println("Ok")
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
            return
        }
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = params
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            
            var msg = "No message"
            

            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                if let parseJSON = json {
                    if let commentaire = parseJSON["cid"] as? String {
                        postCompleted(succeeded: true, data:["msg":"Contenu"])
                    }
                    else{
                        println("Erreur:")
                        postCompleted(succeeded: false, data: ["msg":"Addresse Email déjà utilisée."])
                    }
                    return
                }
                else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    postCompleted(succeeded: false, data: ["msg":"Erreur."])
                }
            }
        })
        
        task.resume()
    }
}

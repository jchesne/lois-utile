import UIKit;
import Realm

class creationViewController:UIViewController, UITextFieldDelegate {
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    

    /* CHAMPS CREATION */
    @IBOutlet weak var prenomLabel: UITextField!
    @IBOutlet weak var nomLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!

    /* CHAMPS CONNECTION */
//    @IBOutlet weak var passwordLabelConnection: UITextField!
//    @IBOutlet weak var emailLabelConnection: UITextField!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad()  {
        
        /* CHAMPS CREATION */
        self.prenomLabel.returnKeyType = UIReturnKeyType.Next
        self.prenomLabel.delegate = self
        self.nomLabel.returnKeyType = UIReturnKeyType.Next
        self.nomLabel.delegate = self
        self.emailLabel.returnKeyType = UIReturnKeyType.Next
        self.emailLabel.delegate = self
        self.passwordLabel.returnKeyType = UIReturnKeyType.Done
        self.passwordLabel.delegate = self
        
        /* CHAMPS CONNECTION */
//        self.emailLabelConnection.returnKeyType = UIReturnKeyType.Next
//        self.emailLabelConnection.delegate = self
//        self.passwordLabelConnection.returnKeyType = UIReturnKeyType.Done
//        self.passwordLabelConnection.delegate = self
 
        super.viewDidLoad()
        title = "Créez votre compte";
        
//        innerViews.append(self.vueCreation)
//        innerViews.append(self.vueConnection)
        
//        var currentView = self.innerViews[0]
//        segmentControl.addTarget(self, action:"segmentSwitch:" , forControlEvents:UIControlEvents.ValueChanged  )
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.prenomLabel) {
            self.nomLabel.becomeFirstResponder()
        }
        else if (textField == self.nomLabel) {
            self.emailLabel.becomeFirstResponder()
        }
        else if (textField == self.emailLabel) {
            self.passwordLabel.becomeFirstResponder()
        }
        else if (textField == self.passwordLabel) {
            // Je cache le keyboard.
            textField.resignFirstResponder()
            creerUser()
        }
        
//        if (textField == self.emailLabelConnection) {
//            self.passwordLabelConnection.becomeFirstResponder()
//        }
//        else if (textField == self.passwordLabelConnection) {
//            textField.resignFirstResponder()
//            
//            var email = emailLabelConnection.text
//            var password = passwordLabelConnection.text
//            //loginUser(email,password: password)
//            datasSingleton.sharedInstance.login_user(email,password: password)
//        }
        return true;
    }
    
//    func segmentSwitch(control:UISegmentedControl) {
//        //TODO: Rajouter un traitement pour éviter de clicker pendant une animation
//            selectView(control.selectedSegmentIndex)
//    }
//    
//    func selectView(index:Int) {
//        var currentView = self.innerViews[self.currentIndex]
//        var nextView = self.innerViews[index]
//
//        animation = true
//        
//        springWithCompletion(0.5, {currentView.alpha = 0},
//            {
//                spring(0.5) {nextView.alpha = 1}
//                self.animation = false
//            }
//        )
//        
//        self.currentIndex = index
//        
//    }

    
    @IBAction func creerUserClicked(sender: UIButton) {
        creerUser()
    }
    
    func creerUser(){
        var nom = nomLabel.text
        var password = passwordLabel.text
        var email = emailLabel.text
        
        self.post(useToken: false,type:"createlogin",params: ["name":nom, "pass":password, "mail":email], url: "\(self.appDelegate.url)/api/v1/fr/user/createlogin") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                // Je viens de crée l'utilisateur, je lui attribut le mot de passe qu'il vient de rensseigner.
                let realm = RLMRealm.defaultRealm()
                realm.beginWriteTransaction()
                var connectedUser = UserModel.getUser()!
                connectedUser.password = password
                realm.commitWriteTransaction()
                datasSingleton.sharedInstance.userFavorites()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                alert.title = "Erreur."
                alert.message = data["msg"]
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    
    func post(useToken:Bool = false,type:String,params : Dictionary<String, String>, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                    if let success = parseJSON["sessid"] as? String {
                        //println("Succes:")
                        var data:[String:String] = ["token":parseJSON["token"] as! String]
                        
                        //Création de l'objet user
                        if let user = parseJSON["user"] as? NSDictionary {
                            
                            let realm = RLMRealm.defaultRealm()
                            realm.beginWriteTransaction()
                            UserModel.createOrUpdateInDefaultRealmWithObject(user)
                            realm.commitWriteTransaction()
                            
                            //Au login du user, je lui met le token.
                            realm.beginWriteTransaction()
                            var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                            connectedUser.token = parseJSON["token"] as! String
                            realm.commitWriteTransaction()
                        }
                        
                        postCompleted(succeeded: true, data: data)
                    }
                    else{
                        println("Erreur:")
                        postCompleted(succeeded: false, data: ["msg":"Une erreur est survenue, verifiez vos champs."])
                    }
                }
                else {

                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr)")
                        postCompleted(succeeded: false, data: ["msg":"Erreur."])

                }
            }
        })
        
        task.resume()
    }

    
    @IBAction func connecterUser(sender: UIButton) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
//
//  citations.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 31/01/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import Foundation
//http://evene.lefigaro.fr/citations/mot.php?mot=lois&p=3

let citations:Array<Dictionary<String,String>> = [
    ["text":"Les lois inutiles affaiblissent les lois nécessaires.","auteur":"Montesquieu"],
    ["text":"La véritable liberté obéit aux lois.","auteur":"Jacques Perk"],
    ["text":"Les meilleures lois naissent des usages","auteur":"Joseph Joubert"],
    ["text":"Il faut éclairer l'histoire par les lois et les lois par l'histoire.","auteur":"Montesquieu"],
    ["text":"Les lois des hommes ne peuvent changer ni muer les lois de nature.","auteur":"Michel de L'Hospital"],
    ["text":"Les lois trop douces ne sont pas suivies, les lois trop sévères ne sont pas appliquées.","auteur":"Benjamin Franklin"],
    ["text":"Les hommes font les lois, les femmes les abrogent.","auteur":"Alphonse Karr"],
    ["text":"Nous sommes esclaves des lois pour pouvoir être libres.","auteur":"Cicéron"],
    ["text":"De mauvaises lois sont la pire sorte de tyrannie.","auteur":"Edmund Burke"],
    ["text":"Quand les lois seront justes, les hommes seront justes.","auteur":"Anatole France"],
    ["text":"L'honnêteté est au-dessus des lois mêmes.","auteur":"Ménandre"],
    ["text":"Celui qui fait exécuter les lois doit y être soumis.","auteur":"Montesquieu"],
    ["text":"Il n'est rien sujet à plus continuelle agitation que les lois.","auteur":"Montaigne"],
    ["text":"L'amour sera toujours au-dessus de la morale et des lois","auteur":"Andrée Maillet"],
    ["text":"La société est bien gouvernée quand les citoyens obéissent aux magistrats et les magistrats aux lois.","auteur":"Solon"],
    ["text":"Chacun a la responsabilité morale de désobéir aux lois injustes.","auteur":"Martin Luther King"],
    ["text":"Il y a une loi avant les lois : pour venir en aide à un humain sans toit, sans pain, privé de soins, il faut braver toutes les lois.","auteur":"Abbé Pierre"],
        
]
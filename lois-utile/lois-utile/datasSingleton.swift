//
//  datasSingleton.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 14/10/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import Foundation
import UIKit
import Realm
import SwiftyJSON

class datasSingleton:UIViewController{
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
   // let managedObjectContext = (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    var idOfFavorites = [String:[String:Any]]()//clef qui est l'id et valeur ["objectIndex":index dans le tableau, "date_ajout":date]
    var idOfVotes = [String:NSNumber]()//Id : Etat
    var currentDownloadTask: NSURLSessionDownloadTask!
//    var categories:NSFetchedResultsController!
//    var fetchedLois:NSFetchedResultsController!
    
    var connectedUSer:UserModel!
    
    var scroll_deja_handled = false;
    
    func setConnectedUser(){
        
    }
    
    class var sharedInstance: datasSingleton {

        struct Static {
            static var instance: datasSingleton?
            static var token: dispatch_once_t = 0
            }
        
            dispatch_once(&Static.token) {
                Static.instance = datasSingleton()
            }
        
        return Static.instance!
    }
    
    func downloadAvatar(){
        if(UserModel.getUser() != nil && UserModel.getUser()?.picture_url != ""){
            // Je télécharge en asynchrone la photo.
            self.currentDownloadTask = RRDownloadFile.download("user_avatar.jpg", downloadSource:
                NSURL(string: UserModel.getUser()!.picture_url)!, pathDestination: NSURL(string: "avatar")!,
                progressBlockCompletion:nil) { (error, fileDestination) -> () in
                    if (error == nil) {
                        println("Téléchargement avatar OK")
                        NSNotificationCenter.defaultCenter().postNotificationName("finishInitUser", object: nil)
                    }
                    else {
                        NSLog("ERROR DOWNLOAD = \(error)")
                    }
            }
        }
    }
    
    /*
    Fonction qui récupère les commentaire de l'utilisateur connecté.
    */
    func userComments(){
        self.post(useToken: true,type:"getUserComments",params: ["":""], url: "\(self.appDelegate.url)/api/v1/fr/user/comments") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                println("Récupération des commentaires de l'utilisateur OK")
                self.downloadAvatar()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                alert.title = "Erreur."
                alert.message = data["msg"]
                // Move to the UI thread
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }

    
    /*
    Fonction qui récupère les favoris de l'user connecté.
    */
    func userFavorites(){
        self.post(useToken: true,type:"getFavorites",params: ["":""], url: "\(self.appDelegate.url)/api/v1/fr/contents/getFavorites") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                println("Récupération des favoris OK")
                self.userComments()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                alert.title = "Erreur."
                alert.message = data["msg"]
                // Move to the UI thread
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    /*
    Foncion qui va demander un nouveau token à Drupal.
    */
    func getToken(callback: () -> Void){
        self.post(useToken: false,type:"getToken",params: ["":""], url: "\(appDelegate.url)/api/v1/fr/user/token") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                
                callback()
                
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    /*
    Foncion qui va modifier le prenom/nom de l'utilisateur.
    */
    func updateUser(firstname:String,lastname:String,callback: () -> Void){
        
        var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel("Enregistrement en cours",options: options)
        
        self.post(useToken: true,type:"updateUser",params: ["firstname":firstname,"lastname":lastname], url: "\(appDelegate.url)/api/v1/fr/user/update_name") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                callback()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                TAOverlay.hideOverlay()
            })
        }
    }
    
    /*
    Foncion qui va envoyer un mail à l'utilisateur pour récuperer le mot de passe oublié.
    */
    func passwordRecovery(email:String,callback: () -> Void){
        var options = (TAOverlayOptions.OverlayTypeActivityDefault | TAOverlayOptions.OverlaySizeRoundedRect)
        TAOverlay.showOverlayWithLabel(nil,options: options)
        
        self.post(useToken: false,type:"passwordRecovery",params: ["name":email], url: "\(appDelegate.url)/api/v1/fr/user/request_new_password") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
           
            dispatch_async(dispatch_get_main_queue(), {
                TAOverlay.hideOverlay()
            })
            
            if(succeeded) {
                callback()
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }

    //no token used
    //fonction apellé uniquement via la connection user/password. 
    func login_user(email:String, password:String, useToken:Bool = false){
        /* Côté drupal, j'intercepte le login et je change la verif du nom par la verif du mail via hook_services_resources_alter() */
        self.post(useToken: useToken,type:"login",params: ["username":email, "password":password], url: "\(appDelegate.url)/api/v1/fr/user/login") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
            if(succeeded) {
                
                let realm = RLMRealm.defaultRealm()
                realm.beginWriteTransaction()
                var connectedUser = UserModel.getUser()!
                connectedUser.password = password
                realm.commitWriteTransaction()
                
                //Je récupère les favoris de l'utilisateur.
                self.userFavorites()
                
                self.downloadAvatar()
                
                NSNotificationCenter.defaultCenter().postNotificationName("finishInitUser", object: nil, userInfo:["message":"Init User Drupal Success"])
            }
            else {
                var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                dispatch_async(dispatch_get_main_queue(), {
                    alert.show()
                })
            }
        }
    }
    
    //token used
    /*
    Fonction qui déconnecte l'utilisateur Drupal / Twitter / Facebook.
    -Suppression de :
    --Favoris
    --UserModel
    --UserComments
    */
    func logout_user(){
        if(UserModel.getUser() != nil){
            self.post(useToken: true,type:"logout",params: ["":""], url: "\(appDelegate.url)/api/v1/fr/user/logout") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    realm.deleteObjects(Favoris.allObjects())
                    realm.deleteObjects(UserModel.allObjects())
                    realm.deleteObjects(UserComments.allObjects())
                    realm.deleteObjects(tempShare.allObjects())
                    realm.commitWriteTransaction()
                    println("Deconnection OK!")
                    println("Suppression des favoris en local. OK")
                    println("Suppression des commentaires de l'utilisateur. OK")
                    println("Suppression de l'utilisateur en local. OK")
                    println("Suppression des authorisation Twitter et Facebook OK")
                    
                    // Si j'ai une session active FB alors je la coupe.
                    if(FB.hasActiveSession()){
                        FB.logout();
                        println("Suppression de la session Facebook. OK")
                    }
                    
                    //@TODO: Suppression de la session Twitter.
                    //@TODO: Suppression de l'avatar
                    
                    /*
                    J'envois à ma page de deconnection le fait qu'on c'est bien déco. 
                    Je vais donc revenir sur la page d'accueil.
                    */
                    dispatch_async(dispatch_get_main_queue(), {
                         NSNotificationCenter.defaultCenter().postNotificationName("finishDestroyUser", object: nil, userInfo:["message":"Destroy User with Success"])
                    })
                   
                    
                    //Je garde les données liées aux votes, à voir par la suite si je les supprimes.
                }
                else {
                    var alert = UIAlertView(title: "Success!", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
        }
    }
    
    func change_vote(etat:Int, lois:Lois){
        
    }
    
    func post(useToken:Bool = false,type:String,params : Dictionary<String, String>, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        if(!Reachability.isConnectedToNetwork()){
            NSNotificationCenter.defaultCenter().postNotificationName("noInternet", object: nil)
            return
        }
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if(useToken == true){
            request.addValue(UserModel.getUser()!.token, forHTTPHeaderField: "X-CSRF-Token")
        }
        
        //println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            //println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            //println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            
       
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"ss"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it

                        //Login ok
                    if(type == "login"){
                        if let success = parseJSON["sessid"] as? String {
                            //println("Succes:")
                            var data:[String:String] = ["token":parseJSON["token"] as! String]
                            
                            //Création de l'objet user
                            if let user = parseJSON["user"] as? NSDictionary {
                                
                                let realm = RLMRealm.defaultRealm()
                                realm.beginWriteTransaction()
                                UserModel.createOrUpdateInDefaultRealmWithObject(user)
                               // println(user)
                                //realm.commitWriteTransaction()
                                
                                println("Mise à jour de l'utilisateur local après login OK")
                                
                                println("Par precaution je supprime les key twitter temp share OK")
                                realm.deleteObjects(tempShare.allObjects())
                                
                                //Au login du user, je lui met le token.
                               // realm.beginWriteTransaction()
                                var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                                connectedUser.token = parseJSON["token"] as! String
                                realm.commitWriteTransaction()
                                
                                println("Attribution du token à l'utilisateur après login OK")
                            }
                            
                            postCompleted(succeeded: true, data: data)
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Une erreur est survenue, verifiez vos champs."])
                        }
                    }
                    // Get favoris
                    else if(type == "getFavorites"){
                        if let favoris = parseJSON["favoris"] as? [NSDictionary] {
                            
                            let json = JSON(parseJSON)
                            
                            let realm = RLMRealm.defaultRealm()
                            
                            realm.beginWriteTransaction()
                            
                            //En 1er je supprime tous les favoris
                            realm.deleteObjects(Favoris.allObjects())
                            
                            for (index: String, subJson: JSON) in json["favoris"] {
                                var favori = Favoris()
                                //favori.fid = subJson["fid"].string!
                                favori.date_ajout = subJson["date_ajout"].string!
                                var nid = subJson["nid"].string!
                                favori.lois = Lois.objectsWhere("nid = '\(nid)'").firstObject() as! Lois
                                Favoris.createInDefaultRealmWithObject(favori)
                            }
                            
                            realm.commitWriteTransaction()
                            
                            
                            postCompleted(succeeded: true, data:["msg":"Favoris mis à jour!"])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Aucun favoris."])
                        }
                    }
                    // Get User comments
                    else if(type == "getUserComments"){
                        if let comments = parseJSON["comments"] as? [NSDictionary] {
                            
                            let realm = RLMRealm.defaultRealm()
                            
                            // Ici je crée les catégories / lois en utilisant Realm qui va instancier et mettre à jour automatiquement mes objets.
                            realm.beginWriteTransaction()
                            for comment in comments {
                                UserComments.createOrUpdateInDefaultRealmWithObject(comment)
                            }
                            
                            realm.commitWriteTransaction()
                            
                            postCompleted(succeeded: true, data:["msg":"Commentaires récupérés OK"])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Aucun favoris."])
                        }
                    }
                    //User Update prenom/nom
                    else if(type == "passwordRecovery"){
                        //var data:[String:String] = ["token":parseJSON["success"] as String]
                        
                        
                        //Au login du user, je lui met le token.
//                        let realm = RLMRealm.defaultRealm()
//                        
//                        realm.beginWriteTransaction()
//                        var connectedUser = UserModel.allObjects().firstObject() as UserModel
//                        connectedUser.prenom = params["firstname"]!
//                        connectedUser.nom = params["lastname"]!
//                        realm.commitWriteTransaction()
                        
                        println("Envois du mail à l'utilisateur OK")
                        
                        postCompleted(succeeded: true, data: ["":""])
                    }
                    //User Update prenom/nom
                    else if(type == "updateUser"){
                        //var data:[String:String] = ["token":parseJSON["success"] as String]
                        
                        
                        //Au login du user, je lui met le token.
                        let realm = RLMRealm.defaultRealm()
                        
                        realm.beginWriteTransaction()
                        var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                        connectedUser.prenom = params["firstname"]!
                        connectedUser.nom = params["lastname"]!
                        realm.commitWriteTransaction()
                        
                        println("Modification du nom/prenom de l'utilisateur OK")
                        
                        postCompleted(succeeded: true, data: ["":""])
                    }
                    //Get Token
                    else if(type == "getToken"){
                        var data:[String:String] = ["token":parseJSON["token"] as! String]
                        
                        //J'applique le token au user
                        
                        //Au login du user, je lui met le token.
                        let realm = RLMRealm.defaultRealm()

                        realm.beginWriteTransaction()
                        var connectedUser = UserModel.allObjects().firstObject() as! UserModel
                        connectedUser.token = parseJSON["token"] as! String
                        realm.commitWriteTransaction()
                        
                        println("Attribution de la generation du token à l'utilisateur OK \(connectedUser.token)")
                        
                        postCompleted(succeeded: true, data: data)
                    }
                    return
                }
                else {
                    if(type == "logout"){
                        postCompleted(succeeded: true, data: ["msg":"Deconnection OK."])
                    }
                    else if(type == "login"){
                        println("Tentative de connection avec l'utilisateur déjà connecté.")
                        postCompleted(succeeded: true, data: ["msg":"Utilisateur déjà connecté."])
                    }
                    else if(type == "passwordRecovery"){
                        
                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        if(jsonStr == "[false]"){
                            postCompleted(succeeded: false, data: ["msg":"Email vide"])
                        }
                        else if(jsonStr == "[true]"){
                            println("Envoie du mail à l'utilisateur OK.")
                            postCompleted(succeeded: true, data: ["msg":"Succes!"])
                        }
                        else{
                            postCompleted(succeeded: false, data: ["msg":"Votre adresse email n'est liée à aucun compte."])
                        }
                    }
                    else{
                        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                        println("Error could not parse JSON: \(jsonStr) \(type)")
                        postCompleted(succeeded: false, data: ["msg":"Erreur."])
                    }
                }
            }
        })
        
        task.resume()
    }
    
}
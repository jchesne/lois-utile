//
//  HeaderCell.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 06/02/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  AppDelegate.swift
//  lois-utile
//
//  Created by Jérôme Chesne on 19/09/2014.
//  Copyright (c) 2014 Jérôme Chesne. All rights reserved.
//

import UIKit
import SwifteriOS
import Realm

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?



    
    #if (arch(i386) || arch(x86_64)) && os(iOS)
    let url = "http://dev.lois-utiles.fr"//EN local
    #else
    let url = "http://dev.lois-utiles.fr"//Via mobile http://dev.lois-utiles.fr
    #endif
    
    let blue_fonce = UIColor(red: 41/255, green: 43/255, blue: 53/255, alpha: 1)
    let blue_clair = UIColor(red: 121/255, green: 189/255, blue: 224/255, alpha: 1)
    let gris = UIColor(red: 225/255, green: 235/255, blue: 245/255, alpha: 1)
    let orange = UIColor(red: 255/255, green: 135/255, blue: 19/255, alpha: 1)
    let gris_button_tabbar_actif = UIColor(red: 59/255, green: 77/255, blue: 79/255, alpha: 1)
    let gris_button_tabbar_inactif = UIColor(red: 119/255, green: 138/255, blue: 140/255, alpha: 1)
    
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window!.backgroundColor = orange

       // UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName:UIFont(name: "OpenSans", size: 18.0)!, NSForegroundColorAttributeName:UIColor.whiteColor()]

        //UINavigationBar.appearance().translucent = true
        UINavigationBar.appearance().barTintColor = orange
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        
        UITabBar.appearance().barTintColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1)
        UITabBar.appearance().tintColor = UIColor(red: 59/255, green: 77/255, blue: 79/255, alpha: 1)
        UITabBar.appearance().selectionIndicatorImage = UIImage(named: "bg_tab_actif")
//        
//        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "OpenSans-Semibold", size: 10.0)!, NSForegroundColorAttributeName:gris_button_tabbar_actif], forState: UIControlState.Selected)
//        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "OpenSans", size: 12.0)!, NSForegroundColorAttributeName:gris_button_tabbar_inactif], forState: UIControlState.Normal)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "Helvetica", size: 14.0)!], forState: .Normal)
        
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "previous")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "previous")
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        //UINavigationBar.appearance().backItem
        // UIBarButtonItem.appearance().setTitleTextAttributes(textDictionary, forState: UIControlState.Normal)
        // Change status bar style
        //UIApplication.sharedApplication().statusBarStyle = .LightContent
    
       // FBLoginView.self
        
        var type = UIUserNotificationType.Badge | UIUserNotificationType.Alert | UIUserNotificationType.Sound;
        var setting = UIUserNotificationSettings(forTypes: type, categories: nil);
        UIApplication.sharedApplication().registerUserNotificationSettings(setting);
        UIApplication.sharedApplication().registerForRemoteNotifications();

        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        println("didRegisterForRemoteNotificationsWithDeviceToken")
        if(deviceModel.geDevice() == nil || deviceModel.geDevice()?.token == ""){
            //Ici je fais une requette post qui va enregistrer le token dans drupal
            var datastring = NSString(data: deviceToken, encoding: UInt())
            
            var deviceToken = deviceToken.description
            deviceToken = deviceToken.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
            deviceToken = deviceToken.stringByReplacingOccurrencesOfString(" ", withString:"", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            self.post(deviceToken, url: "\(self.url)/api/v1/fr/free/push_notifications") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    
                        var device = deviceModel()
                        device.token = deviceToken
                        deviceModel.createInDefaultRealmWithObject(device)
                    
                    realm.commitWriteTransaction()
                    
                    println("Device bien ajouté aux notifications.")
                    
                    // Je refais un appel en mettan à jour la table de configuration des notifications.
                    //self.update_notification_configuration(deviceToken)
                    
                }
                else {
                    
                    var alert = UIAlertView(title: "Erreur", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
        }
    }
    
    func update_notification_configuration(deviceToken:String){
            self.post(deviceToken, url: "\(self.url)/api/v1/fr/free/push_notifications") { (succeeded: Bool, data: Dictionary<String, String>) -> () in
                if(succeeded) {
                    let realm = RLMRealm.defaultRealm()
                    realm.beginWriteTransaction()
                    
                    var device = deviceModel()
                    device.token = deviceToken
                    deviceModel.createInDefaultRealmWithObject(device)
                    
                    realm.commitWriteTransaction()
                    
                    println("Device bien ajouté aux notifications.")
                    
                    // Je refais un appel en mettan à jour la table de configuration des notifications.
                    
                    
                }
                else {
                    
                    var alert = UIAlertView(title: "Erreur", message: data["msg"], delegate: self, cancelButtonTitle: "Okay.")
                    dispatch_async(dispatch_get_main_queue(), {
                        alert.show()
                    })
                }
            }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        println("Couldn't register: \(error)")
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        
        // Je récupère l'url qui viens d'être envoyé
        var debut_url:String = url.absoluteString!.substringToIndex(advance(url.absoluteString!.startIndex, 28))
        if(debut_url == "fb1399147183714013://success"){
            Swifter.handleOpenURL(url)
            return true
        }
        else{
            var wasHandled:Bool = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication!)
            return wasHandled
        }
    }
    
//    func application(application: UIApplication!, openURL url: NSURL!, sourceApplication: String!, annotation: AnyObject!) -> Bool {
//        Swifter.handleOpenURL(url)
//        
//        return true
//    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        println("Je passe 1")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
         println("Je passe 2")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
         println("Je passe 3")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FB.handleDidBecomeActive();
         println("Je passe 4")
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
       // FB.logout();
       // datasSingleton.sharedInstance.logout_user()
     println("Je passe 5")
    }
    
    func post(token : String, url : String, postCompleted : (succeeded: Bool, data: Dictionary<String, String>) -> ()) {
        
        if !Reachability.isConnectedToNetwork() {
            NSNotificationCenter.defaultCenter().postNotificationName("noInternetAlert", object: nil)
            return
        }
        
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var err: NSError?
        
        var params = ["token":token,"type":"ios"]
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        println(request)
        
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            println("Response: \(response)")
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            var msg = "No message"
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                postCompleted(succeeded: false, data:["msg":"Erreur de requette"])
            }
            else {
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                        if let success = parseJSON["success"] as? Int {
                            postCompleted(succeeded: true, data:["Success":"Success"])
                        }
                        else{
                            println("Erreur:")
                            postCompleted(succeeded: false, data: ["msg":"Erreur dans l'initalisation du token"])
                        }
                    return
                }
                else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    postCompleted(succeeded: false, data: ["msg":"Erreur."])
                }
            }
        })
        
        task.resume()
    }
}


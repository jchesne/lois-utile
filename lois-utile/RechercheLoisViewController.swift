import UIKit
import Realm

class RechercheLoisViewController : UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    
//    var filteredCandies:NSFetchedResultsController = NSFetchedResultsController()
//    var fetchedLois:NSFetchedResultsController!
    var pair:Bool = true
    
    var liste_Lois = Lois.allObjects().sortedResultsUsingProperty("changed", ascending: false)
    var liste_Lois_recherche = Lois.allObjects().sortedResultsUsingProperty("changed", ascending: false)
    
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    //let managedObjectContext = (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    
    //@IBOutlet var tableViewController: UITableView!
    
    override func viewDidLoad() {
        
        var textfield:UITextField = searchBar.valueForKey("searchField") as! UITextField
        
        //Set the foregroundcolor of the placeholder
        var attributedString = NSAttributedString(string: "your placeholdertext", attributes: [NSForegroundColorAttributeName : UIColor.redColor()])
        
        textfield.attributedPlaceholder = attributedString
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        tableView.registerNib(UINib(nibName: "ListLoisCell", bundle: nil), forCellReuseIdentifier: "ListLoisCell")
        
        //self.fetchedLois = datasSingleton.sharedInstance.fetchedLois
        self.tableView.reloadData()
        
        //self.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
        //self.
//        self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.None, animated: false)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            if(liste_Lois_recherche.count == 0){
                return 0
            }
            else{
                return Int(liste_Lois_recherche.count)
            }

        } else {
            return Int(self.liste_Lois.count)
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //ask for a reusable cell from the tableview, the tableview will create a new one if it doesn't have any
        let cell = self.tableView.dequeueReusableCellWithIdentifier("ListLoisCell", forIndexPath: indexPath) as! ListLoisCell
        var lois : Lois!
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            lois = liste_Lois_recherche.objectAtIndex(UInt(indexPath.row)) as! Lois
        } else {
            lois = liste_Lois.objectAtIndex(UInt(indexPath.row)) as! Lois
        }
        
        cell.body.text = lois.body
        cell.titre.text = lois.title
      //  cell.categorie.text = lois.categorie_name
        cell.date_ajout.text = "ajouté le \(lois.changed) dans TOTO"
        cell.nombre_commentaires.text = "\(lois.comments_count)"
        cell.vote_plus.setTitle("\(lois.vote_plus)", forState: UIControlState.Normal)
        
        //cell.layoutIfNeeded()
        
        return cell
    }
    
    func filterContentForSearchText(searchText: String) {
        let predicate = NSPredicate(format: "title contains[c] %@", searchText)

        self.liste_Lois_recherche = Lois.objectsWithPredicate(predicate).sortedResultsUsingProperty("changed", ascending: false)
    }
    
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("ListeRechercheToDetail", sender: tableView)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "ListeRechercheToDetail" {
            let indexPath: AnyObject? = self.tableView.indexPathForSelectedRow()
            let detailLoisViewController = segue.destinationViewController as! UILoisPageViewController
            
            var selectedIndex:NSIndexPath!
            //detailLoisViewController.ListeLoisParCategorie = Lois.objectsWhere("nid = '\(selectedLois.nid)'")
            if sender as! UITableView == self.searchDisplayController!.searchResultsTableView {
                 selectedIndex = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
                detailLoisViewController.ListeLoisParCategorie = self.liste_Lois_recherche
            } else {
                selectedIndex = self.tableView.indexPathForSelectedRow()!
                detailLoisViewController.ListeLoisParCategorie = self.liste_Lois
            }
            
            detailLoisViewController.currentIndex = selectedIndex.row
        }
    }
    
    
    
}

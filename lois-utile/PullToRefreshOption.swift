//
//  PullToRefreshConst.swift
//  PullToRefreshSwift
//
//  Created by Yuji Hato on 12/11/14.
//
import UIKit

struct PullToRefreshConst {
    static let tag = 810
    static let alpha = true
    static let height: CGFloat = 50
    static let imageName: String = "pulltorefresharrow.png"
    static let animationDuration: Double = 0.4
    static let fixedTop = true // PullToRefreshView fixed Top
}

//let options = PullToRefreshOption()
//options.backgroundColor = UIColor.blueColor()
//options.indicatorColor = UIColor.whiteColor()
//self.tableView.addPullToRefresh(options: options, { [weak self] in
class PullToRefreshOption {
    var backgroundColor = UIColor(red: 245/255, green: 248/255, blue: 250/255, alpha: 1)
    var indicatorColor = UIColor.grayColor()
    var autoStopTime: Double = 0.7 // 0 is not auto stop
    var fixedSectionHeader = false  // Update the content inset for fixed section headers
}
//
//  switchContentProfilButton.swift
//  lois-utiles
//
//  Created by Jérôme Chesne on 22/03/2015.
//  Copyright (c) 2015 Jérôme Chesne. All rights reserved.
//

import UIKit

@IBDesignable class switchContentProfilButton: UIView {

    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var imageViewActive: UIImageView!
    
    var isActive:Bool = false{
        didSet{
            setupView()
        }
    }

    
    // Our custom view from the XIB file
    var view: UIView!
    var currentIndex:Int!
    
    @IBInspectable var labelText:String = "" {
        didSet{
            setupView()
        }
    }
    
    @IBInspectable var image: UIImage? {
        get {
            return imageView.image
        }
        set(image) {
            imageView.image = image
        }
    }
    
    @IBInspectable var imageActive: UIImage? {
        didSet{
            setupView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView(){
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
        self.text.text = labelText

        if(isActive){
            self.imageView.image = self.imageActive
        }
        else{
            self.imageView.image = self.image
        }
        
        self.setNeedsDisplay()
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "switchContentProfilButton", bundle: bundle)
        
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    @IBAction func changeDataCliked(sender: UIButton) {
        NSNotificationCenter.defaultCenter().postNotificationName("changeData", object:nil, userInfo:["index":self.tag])
    }
    
    
}
